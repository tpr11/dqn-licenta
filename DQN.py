import numpy as np
import tensorflow as tf
from functools import partial
import time

class DQN:

    def __init__(self, input_shape, num_actions, network_shapes=[[[8,8,4,32], [1,4,4,1]], [[4,4,32,64], [1,2,2,1]], [[3,3,64,64], [1,1,1,1]], 512], graph=tf.Graph()):

        tf.set_random_seed(123456)

        self.input_shape = input_shape
        self.num_actions = num_actions
        self.gamma = 0.99

        self.graph = graph
        with self.graph.as_default():
            self.sess = tf.Session()

            self.x = tf.placeholder(tf.uint8, input_shape, name='screens')
            self.normalized = tf.to_float(self.x) / 255.0
            
            self.network_shapes = network_shapes
            self.network_shapes.insert(0, input_shape)
            self.network_shapes.append(num_actions)
            self.y = self._build_network('policy', self.normalized, self.network_shapes, True)
            self.y_ = self._build_network('target', self.normalized, self.network_shapes, False)

            self.update_target_model = []
            self.trainable_vars = tf.trainable_variables()
            global_vars = tf.global_variables()
            for i in range(len(self.trainable_vars)):
                match = list(filter(lambda var: var is not self.trainable_vars[i] and var.name.split('/')[1:] == self.trainable_vars[i].name.split('/')[1:], global_vars))[0]
                self.update_target_model.append(tf.assign(match, self.trainable_vars[i]))

            self.a = tf.placeholder(tf.float32, [None, self.num_actions], name='actions')
            self.y_a = tf.multiply(self.y, self.a)
            self.y_a_ = tf.multiply(self.y_, self.a)

            self.learning_rate = 0.00025
            self.manual_update_coeff = tf.placeholder(tf.float32, [])
            self.manual_update_op = []
            for var in self.trainable_vars:
                self.manual_update_op.append(tf.assign(var, var + self.learning_rate * self.manual_update_coeff * tf.gradients(self.y_a, var)[0]))

            self.t = tf.placeholder(tf.float32, [None], name='targets')
            diff = tf.abs(tf.reduce_sum(self.y_a, axis=1) - self.t)
            quadratic = tf.clip_by_value(diff, 0.0, 1.0)
            linear = diff - quadratic
            errors = (0.5 * tf.square(quadratic)) + linear
            self.loss = tf.reduce_sum(errors)

            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate, decay=0.95, epsilon=0.01, name='optimizer')
            self.train_step = self.optimizer.minimize(self.loss)
            
            self.saver = tf.train.Saver(max_to_keep=50)

            with tf.name_scope('summaries'):
                tf.summary.scalar('Q_vals', tf.reduce_mean(tf.multiply(self.y, self.a)))
                tf.summary.scalar('loss', self.loss)
            self.merged = tf.summary.merge_all()
            self.writer = tf.summary.FileWriter('tensorboard/', self.sess.graph)

            self.sess.run(tf.global_variables_initializer())
    
    def _build_network(self, name, input, shapes, trainable):
        with tf.variable_scope(name):
            with tf.variable_scope('conv1'):
                a1 = self._get_activated(shapes[1][0], 
                                         activation=tf.nn.relu, 
                                         op=partial(tf.nn.conv2d, input=input, strides=shapes[1][1], padding='VALID'),
                                         argname='filter', 
                                         trainable=trainable)
            with tf.variable_scope('conv2'):
                a2 = self._get_activated(shapes[2][0], 
                                         activation=tf.nn.relu, 
                                         op=partial(tf.nn.conv2d, input=a1, strides=shapes[2][1], padding='VALID'), 
                                         argname='filter', 
                                         trainable=trainable)
            with tf.variable_scope('conv3'):
                a3 = self._get_activated(shapes[3][0], 
                                         activation=tf.nn.relu, 
                                         op=partial(tf.nn.conv2d, input=a2, strides=shapes[3][1], padding='VALID'), 
                                         argname='filter', 
                                         trainable=trainable)
            w, h = shapes[0][1], shapes[0][2]
            for i in range(1,4):
                w, h = (w - shapes[i][0][0]) / shapes[i][1][1] + 1, (h - shapes[i][0][1]) / shapes[i][1][2] + 1
            flat = tf.reshape(a3, [-1, w*h*shapes[3][0][-1]], name='flattened')
            with tf.variable_scope('fc1'):
                a4 = self._get_activated([w*h*shapes[3][0][-1], shapes[4]], 
                                         activation=tf.nn.relu, 
                                         op=partial(tf.matmul, a=flat),
                                         trainable=trainable)
            with tf.variable_scope('fc2'):
                Q_vals = self._get_activated([shapes[4], shapes[5]], 
                                             activation=tf.identity, 
                                             op=partial(tf.matmul, a=a4),
                                             trainable=trainable)
        return Q_vals

    def _get_activated(self, shape, activation, op, argname='b', trainable=True):
        w = tf.Variable(tf.truncated_normal(shape, stddev=0.01), trainable=trainable, name='w')
        b = tf.Variable(tf.fill([shape[-1]], 0.1), trainable=trainable, name='b')
        a = activation(op(**{argname: w}) + b, name='a')
        return a

    def predict(self, states, target=False):
        with self.graph.as_default():
            return self.sess.run(self.y, feed_dict={self.x: states})

    @staticmethod
    def to_one_hot(a, v_max):
        b = np.zeros((a.size, v_max+1))
        b[np.arange(a.size), a] = 1
        return b

    def manual_backprop(self, batch):
        start_states, actions, rewards, next_states, terminal, w = batch
        with self.graph.as_default():
            Q_vals_ = self.sess.run(self.y_a, feed_dict={self.x: next_states, self.a: np.ones(actions.shape)})
            Q_vals = self.sess.run(self.y_a, feed_dict={self.x: start_states, self.a: actions})
            delta = self.sess.run(self.y_a_, feed_dict={self.x: next_states, self.a: DQN.to_one_hot(np.argmax(Q_vals_, axis=1), actions.shape[1]-1)})
            delta[terminal] = 0
            delta = rewards + np.sum(self.gamma * delta - Q_vals, axis=1)
            self.sess.run(self.manual_update_op, feed_dict={self.x: start_states, self.a: actions, self.manual_update_coeff: np.sum(w * delta)})
        return delta

    def train(self, batch):
        start_states, actions, rewards, next_states, terminal = batch
        with self.graph.as_default():
            Q_vals = self.sess.run(self.y_, feed_dict={self.x: next_states})
            Q_vals[terminal] = 0
            Q_vals = rewards + self.gamma * np.max(Q_vals, axis=1)
            feed_dict = {self.x: start_states, self.a: actions, self.t: Q_vals}
            self.sess.run([self.train_step, self.merged], feed_dict=feed_dict)

    def update_target(self):
        with self.graph.as_default():
            self.sess.run(self.update_target_model)

    def save(self, path, step):
        with self.graph.as_default():
            self.saver.save(self.sess, path, global_step=step)

    def restore(self, path):
        with self.graph.as_default():
            self.saver.restore(self.sess, path)