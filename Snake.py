import sys
import os
import sys
import random
import time
import argparse
import signal
import copy

import numpy as np
import tensorflow as tf
import scipy.ndimage
import pygame
from PIL import Image

from DQN import DQN
from Memory import Memory

class Unit:

    class Roles:
        PLAYER = 11
        PLAYER1 = 11
        PLAYER2 = 22
        FOOD = 1

    class Part:
        def __init__(self, x, y):
            self.x, self.y = x, y
        def __eq__(self, other):
            return self.x == other.x and self.y == other.y
        def __repr__(self):
            return '(%d, %d)' % (self.x, self.y)

    def __init__(self, role, parts, part_size, margins=None, direction=None, speed=None, actions=None, alive=False, ai=False):
        self.role = role
        self.direction = direction
        self.parts = zip(parts, parts[1:]) if len(parts) > 1 else [(parts[0], parts[0])]
        self.part_width, self.part_height, _ = part_size
        if margins:
            self.width, self.height = margins
        self.speed = speed
        self.actions = actions
        self.alive = alive
        self.time_created = time.time()
        self.ai = ai
        self.do_extend = False
        self.reset_parts = copy.deepcopy(self.parts)
        self.reset_direction = copy.deepcopy(self.direction)
        self.reset_alive = copy.deepcopy(self.alive)

    def reset(self):
        self.parts = copy.deepcopy(self.reset_parts)
        self.direction = copy.deepcopy(self.reset_direction)
        self.alive = copy.deepcopy(self.reset_alive)
        self.do_extend = False

    def change_direction(self, direction):
        if direction % 2 == self.direction % 2:
            return
        self.direction = direction

    def move(self):
        if self.parts[0][0].x == self.parts[0][1].x:
            if self.parts[0][0].y <= self.parts[0][1].y:
                previous_direction = 0
            else:
                previous_direction = 2
        elif self.parts[0][0].y == self.parts[0][1].y:
            if self.parts[0][0].x <= self.parts[0][1].x:
                previous_direction = 3
            else:
                previous_direction = 1
        if previous_direction != self.direction:
            self.parts.insert(0, (Unit.Part(self.parts[0][0].x, self.parts[0][0].y), Unit.Part(self.parts[0][0].x, self.parts[0][0].y)))
        if self.direction is 0:
            self.parts[0][0].y -= self.speed
        elif self.direction is 1:
            self.parts[0][0].x += self.speed
        elif self.direction is 2:
            self.parts[0][0].y += self.speed
        elif self.direction is 3:
            self.parts[0][0].x -= self.speed
        if not self.do_extend:
            self.parts[-1][1].x += np.sign(self.parts[-1][0].x - self.parts[-1][1].x) * self.speed
            self.parts[-1][1].y += np.sign(self.parts[-1][0].y - self.parts[-1][1].y) * self.speed
        else:
            self.do_extend = False
        if self.parts[-1][0] == self.parts[-1][1]:
            self.parts.pop()

    def extend(self):
        self.do_extend = True

class Snake:

    class Events:
        DEATH = pygame.USEREVENT + 1
        EAT = pygame.USEREVENT + 2

    def saver(self, *args, **kwargs):
        print('SIGINT. Saving...')
        if self.args.model_save_path:
            self.model.save(self.args.model_save_path, self.memory.iteration)
        if self.args.memory_save_path: 
            self.memory.save(self.args.memory_save_path)
        sys.exit(0)

    def __init__(self, args):
        
        self.args = args

        self.width, self.height = args.width, args.height
        self.framerate = args.framerate
        self.training = self.args.training
        self.game_actions = ['NOTHING', 'UP', 'DOWN', 'LEFT', 'RIGHT']
        self.game_started = False

        self.background_color = np.array([0, 0, 0], dtype=np.uint8)

        self.player_part_shape = (int(self.width/80), int(self.height/80), 3)
        self.player_speed = int(self.width/80)
        self.player1_color = np.array([65, 125, 175], dtype=np.uint8)
        self.player2_color = np.array([255, 225, 100], dtype=np.uint8)

        self.food_shape = (int(self.width/80), int(self.height/80), 3)
        self.food_color = np.array([255, 255, 255], dtype=np.uint8)

        pygame.init()
        self.display = pygame.display.set_mode((self.width, self.height))

        self.score_font = pygame.font.Font(args.font_path, int(0.075*self.width))
        self.score_color = (255, 255, 255)
        self.score_position = (int(0.475 * self.width), int(0.0625 * self.height))
        
        self.s_background = pygame.surfarray.make_surface(np.ones((self.width, self.height, 3)) * self.background_color)
        self.s_player1_part = pygame.surfarray.make_surface(np.ones(self.player_part_shape) * self.player1_color)
        self.s_player2_part = pygame.surfarray.make_surface(np.ones(self.player_part_shape) * self.player2_color)
        self.s_cover = pygame.surfarray.make_surface(np.ones(self.player_part_shape) * self.background_color)
        self.s_food = pygame.surfarray.make_surface(np.ones(self.food_shape) * self.food_color)

        self.score = [0, 0]
        self.default_timer = 30

        self.grayscale = self.grayscale = np.array([.299, .587, .114])
        self.ndzoom_shape = (1.0 * self.args.image_shape[1]/self.height, 1.0 * self.args.image_shape[2]/self.width)

        if self.training:
            self.epsilon = lambda i: max(0.01, 1 - 0.99 * i / 1000000) 
        else:
            self.epsilon = lambda _: self.args.test_epsilon
        
        self.init_units()

        signal.signal(signal.SIGINT, self.saver)
        
        if self.training:
            while True:
                starting_iteration = self.memory.iteration
                strs = ([], [])
                while self.memory.iteration - starting_iteration < self.args.train_steps:
                    iters = self.memory.iteration
                    self.active_player = self.player1
                    self.player1.total_reward = 0
                    self.player1.acting = 0
                    self.player1.s0 = None
                    self.player2.total_reward = 0
                    self.player2.acting = 0
                    self.player2.s0 = None
                    t0 = time.time()
                    self.start()
                    t1 = time.time()
                    self.memory.episode += 1
                    strs[0].append('PLAYER 1    EP %d    IT %d (%5.2f%%)    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, 100.0 * (self.memory.iteration - starting_iteration) / self.args.train_steps, self.player1.total_reward, t1 - t0, (self.memory.iteration - iters) / (t1 - t0)))
                    strs[1].append('PLAYER 2    EP %d    IT %d (%5.2f%%)    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, 100.0 * (self.memory.iteration - starting_iteration) / self.args.train_steps, self.player2.total_reward, t1 - t0, (self.memory.iteration - iters) / (t1 - t0)))
                    if self.args.verbose:
                        print(strs[0][-1])
                        print(strs[1][-1])
                        print(self.score) 
                try:
                    if self.args.log:
                        with open(self.args.log, 'a') as f:
                            f.write('\n'.join(['%s\n%s' % (str0, str1) for (str0, str1) in zip(strs[0], strs[1])]) + '\n')
                except Exception as e:
                    print('Coulnd\'t save log: ' + e.message)
        else:
            self.start()

    def get_random_food(self):
        screen = self.get_screen().mean(axis=2)[:-self.food_shape[0], :-self.food_shape[1]]
        mask = np.reshape(screen == 0, np.prod(screen.shape))
        vals = np.arange(mask.size)[mask]
        val = vals[np.random.randint(vals.size)]
        return Unit(Unit.Roles.FOOD, [Unit.Part(val % (self.height-self.food_shape[1]), int(val / (self.height-self.food_shape[1])))], self.food_shape)

    def get_screen(self):
        return np.transpose(pygame.surfarray.array3d(self.display), axes=[1, 0, 2])

    def preprocess(self, img):
        frame = np.dot(img, self.grayscale).astype(np.uint8)
        frame = scipy.ndimage.zoom(frame, self.ndzoom_shape)
        frame = np.resize(frame, (self.args.image_shape[1], self.args.image_shape[2], 1))
        return frame

    def init_units(self):
        self.player1 = Unit(Unit.Roles.PLAYER1, [Unit.Part(0.2 * self.width, 0.2 * self.height), Unit.Part(0.2 * self.width, 0.2 * self.height - 2 * self.player_part_shape[1])], self.player_part_shape, margins=(self.width, self.height), direction=2, speed=self.player_speed, alive=True, ai=False)
        self.player2 = Unit(Unit.Roles.PLAYER2, [Unit.Part(0.8 * self.width, 0.8 * self.height), Unit.Part(0.8 * self.width, 0.8 * self.height + 2 * self.player_part_shape[1])], self.player_part_shape, margins=(self.width, self.height), direction=0, speed=self.player_speed, alive=True, ai=False)
        self.units = [self.player1, self.player2]
        for _ in range(2):
            self.units.append(self.get_random_food())
        if self.args.player1 != 'HUMAN' or self.args.player2 != 'HUMAN':
            self.render()
            self.state = []
            for _ in range(self.args.state_size):
                self.state.append(self.preprocess(self.get_screen()))
            if self.args.memory:
                self.memory = Memory.load(self.args.memory)
            else:
                self.memory = Memory(self.args.memory_size, len(self.game_actions))
            self.model = DQN(self.args.image_shape, len(self.game_actions))
            if self.args.model:
                self.model.restore(self.args.model)
        if self.args.player1 != 'HUMAN':
            self.player1.ai = True
            self.player1.total_reward = 0
            self.player1.acting = 0
            self.player1.s0 = None
        else:
            self.player1.actions = {'UP': pygame.K_w, 'RIGHT': pygame.K_d, 'DOWN': pygame.K_s, 'LEFT': pygame.K_a}
        if self.args.player2 != 'HUMAN':
            self.player2.ai = True
            self.player2.total_reward = 0
            self.player2.acting = 0
            self.player2.s0 = None
        else:
            self.player2.actions = {'UP': pygame.K_UP, 'RIGHT': pygame.K_RIGHT, 'DOWN': pygame.K_DOWN, 'LEFT': pygame.K_LEFT}

    def manage_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.game_started = False
                self.timer = 0
            elif event.type == Snake.Events.EAT:
                event.player.extend()
                if event.player is self.player1:
                    self.score[0] += 1
                    if self.training:
                        self.player1.r += 1
                elif event.player is self.player2:
                    self.score[1] += 1
                    if self.training:
                        self.player2.r += 1
                for (i, unit) in enumerate(self.units):
                    if unit is event.food:
                        self.units.pop(i)
                        break
                self.units.append(self.get_random_food())
            elif event.type == Snake.Events.DEATH:
                event.player.alive = False
                if self.training:
                    self.active_player = self.player2 if self.player2 is self.active_player else self.player1
                    event.player.r -= 1
                    event.player.acting = self.args.frame_skip
                if not self.player1.alive and not self.player2.alive:
                    self.new_life = True
                    self.game_started = False
                    if not self.training:
                        self.timer = -1
                    else:
                        self.player1.reset()
                        self.player2.reset()
                        self.active_player = self.player1
                        self.units = [self.player1, self.player2]
                        for _ in range(2):
                            self.units.append(self.get_random_food())
                        self.timer = 0
                        self.render()
                        self.state =[]
                        for _ in range(self.args.state_size):
                            self.state.append(self.preprocess(self.get_screen()))
        for i, unit in enumerate(self.units):
            if unit.role is Unit.Roles.FOOD:
                if time.time() - unit.time_created > 30:
                    self.units[i] = self.get_random_food()

    def deterministic_ai(self, unit):
        valid_dirs = [True, True, True, True]
        valid_dirs[(unit.direction + 2) % 4] = False
        valid_places = []
        places_cost = []
        closest, closest_index = 999999999, -1
        for i, other_unit in enumerate(self.units):
            if other_unit.role != Unit.Roles.FOOD:
                continue
            dist = np.sqrt((other_unit.parts[0][0].x - unit.parts[0][0].x)**2 + (other_unit.parts[0][0].y - unit.parts[0][0].y)**2)
            if dist < closest:
                closest = dist
                closest_index = i
        for i in range(len(valid_dirs)):
            if valid_dirs[i]:
                if i == 0:
                    hfr = Unit.Part(unit.parts[0][0].x, unit.parts[0][0].y - unit.speed)
                elif i == 1:
                    hfr = Unit.Part(unit.parts[0][0].x + unit.speed, unit.parts[0][0].y)
                elif i == 2:
                    hfr = Unit.Part(unit.parts[0][0].x, unit.parts[0][0].y + unit.speed)
                elif i == 3:
                    hfr = Unit.Part(unit.parts[0][0].x - unit.speed, unit.parts[0][0].y)
                hto = Unit.Part(hfr.x + unit.part_width, hfr.y + unit.part_height)
                if hfr.y < 0 or \
                    hto.x >= self.width or \
                    hto.y >= self.height or \
                    hfr.x < 0:
                    valid_dirs[i] = False
                    continue
                valid_places.append((i, hfr, hto))
                places_cost.append(np.sqrt((hfr.x - self.units[closest_index].parts[0][0].x)**2 + (hfr.y - self.units[closest_index].parts[0][0].y)**2))
        for other_unit in self.units:
            if other_unit.role % Unit.Roles.PLAYER != 0:
                continue
            for (i, part) in enumerate(other_unit.parts):
                if other_unit is unit and i < 2:
                    continue
                fr, to = Snake.get_rect(*part, part_width=other_unit.part_width, part_height=other_unit.part_height)
                to.x, to.y = fr.x + to.x, fr.y + to.y
                if i == 0:
                    cfr = copy.deepcopy(fr)
                    cfr.x += (-1)**(other_unit.direction == 0 or other_unit.direction == 3) * (other_unit.direction % 2) * other_unit.speed
                    cfr.y += (-1)**(other_unit.direction == 1 or other_unit.direction == 2) * (other_unit.direction % 2 - 1) * other_unit.speed
                    cto = Unit.Part(cfr.x + other_unit.part_width, cfr.y + other_unit.part_height)
                else:
                    cfr, cto = None, None
                for j, hfr, hto in valid_places:
                    if Snake.rect_overlap((hfr, hto), (fr, to)) or \
                        (cfr and cto and Snake.rect_overlap((hfr, hto), (cfr, cto))):
                        valid_dirs[j] = False
        closest, closest_index = 999999999, -1
        for j, (i, hfr, hto) in enumerate(valid_places):
            if valid_dirs[i]:
                if places_cost[j] < closest:
                    closest = places_cost[j]
                    closest_index = i
        if closest_index == 0:
            return 1
        elif closest_index == 1:
            return 4
        elif closest_index == 2:
            return 2
        elif closest_index == 3:
            return 3
        return 0

    def manage_input(self):
        keys = pygame.key.get_pressed()
        for unit in self.units:
            if unit.alive:
                if unit.role % Unit.Roles.PLAYER is 0:
                    if unit.ai:
                        if unit.acting == self.args.frame_skip:
                            if not unit.alive or not self.game_started:
                                unit.terminal = True 
                            if unit is self.player2:
                                unit.frame[np.where((unit.frame == self.player1_color).all(axis=2))] = [0, 255, 0]
                                unit.frame[np.where((unit.frame == self.player2_color).all(axis=2))] = self.player1_color
                                unit.frame[np.where((unit.frame == [0, 255, 0]).all(axis=2))] = self.player2_color
                            if unit.s0 is None:
                                unit.s1 = self.state[:-1]
                            else:    
                                unit.s1 = unit.s0[:-1]
                            unit.s1.insert(0, self.preprocess(unit.frame))
                            if self.training and unit is self.active_player and unit.s0 is not None:
                                self.memory.add((unit.s0, unit.a, unit.r, unit.s1, unit.terminal))
                                if (self.args.memory_init is None or self.memory.iteration > self.args.memory_init) and \
                                   self.args.train_wait is not None and self.memory.iteration % self.args.train_wait is 0:
                                    # batch = self.memory.sample_prioritized(self.args.batch_size)
                                    # delta = self.model.manual_backprop(batch[:-1])
                                    # self.memory.update_priorities(batch[-1], delta)
                                    self.model.train(self.memory.sample(self.args.batch_size))
                            if unit.terminal:
                                unit.s0 = None
                            else:
                                unit.s0 = unit.s1
                            if self.training and unit is self.active_player and (self.args.memory_init is None or self.memory.iteration > self.args.memory_init):
                                if self.args.model_save_path and self.memory.iteration % self.args.model_save_freq is 0:
                                    if self.args.verbose: print('Saving model.')
                                    self.model.save(self.args.model_save_path, self.memory.iteration)
                                if self.args.memory_save_path and self.memory.iteration % self.args.memory_save_freq is 0:
                                    if self.args.verbose: print('Saving memory.')
                                    self.memory.save(self.args.memory_save_path)
                                if self.args.model_update_freq is not None and self.memory.iteration % self.args.model_update_freq is 0:
                                    if self.args.verbose: print('Updating target models.')
                                    self.model.update_target()
                            unit.total_reward += unit.r
                            unit.acting = 0
                            if self.training and unit is self.active_player:
                                self.memory.iteration += 1
                        elif unit.acting > 0:
                            unit.frame = self.get_screen()
                            unit.acting += 1
                        if unit.acting == 0:
                            if unit.s0 is None or random.random() < self.epsilon(self.memory.iteration):
                                unit.a = random.randrange(len(self.game_actions))
                            else:
                                unit.a = self.deterministic_ai(unit)
                                #s = np.reshape(np.concatenate(unit.s0, axis=2), (1, self.args.image_shape[1], self.args.image_shape[2], self.args.state_size))
                                #unit.a = np.argmax(self.model.predict(s), axis=1)[0]
                            unit.frame = self.get_screen()
                            unit.r = 0
                            unit.terminal = False
                            if self.game_actions[unit.a] is 'UP':
                                unit.change_direction(0)
                            elif self.game_actions[unit.a] is 'RIGHT':
                                unit.change_direction(1)
                            elif self.game_actions[unit.a] is 'DOWN':
                                unit.change_direction(2)
                            elif self.game_actions[unit.a] is 'LEFT':
                                unit.change_direction(3)
                            unit.acting += 1
                    else:
                        if keys[unit.actions['UP']]:
                            unit.change_direction(0)
                        elif keys[unit.actions['RIGHT']]:
                            unit.change_direction(1)
                        elif keys[unit.actions['DOWN']]:
                            unit.change_direction(2)
                        elif keys[unit.actions['LEFT']]:
                            unit.change_direction(3)

    @staticmethod
    def rect_overlap((A1, A2), (B1, B2)):
        return A1.x < B2.x and A2.x > B1.x and \
               A1.y < B2.y and A2.y > B1.y 

    @staticmethod
    def get_rect(part1, part2, part_width, part_height):
        if part1.x == part2.x:
            if part1.y <= part2.y:
                fr, sz = copy.deepcopy(part1), copy.deepcopy(part2)
            else:
                fr, sz = copy.deepcopy(part2), copy.deepcopy(part1)
            sz.x = part_width
            sz.y += part_height - fr.y
        if part1.y == part2.y:
            if part1.x <= part2.x:
                fr, sz = copy.deepcopy(part1), copy.deepcopy(part2)
            else:
                fr, sz = copy.deepcopy(part2), copy.deepcopy(part1)
            sz.y = part_height
            sz.x += part_width - fr.x
        return fr, sz

    def manage_collisions(self):
        for unit in self.units:
            if unit.alive:
                unit.move()
        for unit in self.units:
            if unit.alive:
                hfr = copy.deepcopy(unit.parts[0][0])
                hto = Unit.Part(hfr.x + unit.part_width, hfr.y + unit.part_height)
                if hfr.y < 0 or \
                   hto.x >= self.width or \
                   hto.y >= self.height or \
                   hfr.x < 0:
                    pygame.event.post(pygame.event.Event(Snake.Events.DEATH, player=unit))
                    continue
                for other_unit in self.units:
                    if other_unit.role % Unit.Roles.PLAYER != 0 or \
                       (other_unit is not unit and self.immunity > 0):
                        continue
                    found = False
                    for (i, part) in enumerate(other_unit.parts):
                        if other_unit is unit and i < 2:
                            continue
                        fr, to = Snake.get_rect(*part, part_width=other_unit.part_width, part_height=other_unit.part_height)
                        to.x, to.y = fr.x + to.x, fr.y + to.y
                        if Snake.rect_overlap((hfr, hto), (fr, to)):
                            if i != 0 or (other_unit.parts[0][0].x != hfr.x and other_unit.parts[0][0].y != hfr.y):
                                self.immunity = self.framerate
                            pygame.event.post(pygame.event.Event(Snake.Events.DEATH, player=unit))
                            found = True
                            break
                if not found:
                    for other_unit in self.units:
                        if other_unit.role is not Unit.Roles.FOOD:
                            continue
                        if (other_unit.parts[0][0].x <= hfr.x <= other_unit.parts[0][0].x + other_unit.part_width or \
                            other_unit.parts[0][0].x <= hfr.x + unit.part_width <= other_unit.parts[0][0].x + other_unit.part_width) and \
                           (other_unit.parts[0][0].y <= hfr.y <= other_unit.parts[0][0].y + other_unit.part_height or \
                            other_unit.parts[0][0].y <= hfr.y + unit.part_height <= other_unit.parts[0][0].y + other_unit.part_height):
                            pygame.event.post(pygame.event.Event(Snake.Events.EAT, player=unit, food=other_unit))

    def render(self):
        self.display.blit(self.s_background, (0,0))
        for unit in self.units:
            if unit.role % Unit.Roles.PLAYER == 0:
                for part in unit.parts:
                    fr, sz = Snake.get_rect(*part, part_width=unit.part_width, part_height=unit.part_height)
                    pygame.draw.rect(self.display, tuple(self.player1_color if unit is self.player1 else self.player2_color), pygame.Rect(fr.x, fr.y, sz.x, sz.y))
        for unit in self.units:
            if unit.role is Unit.Roles.FOOD:
                self.display.blit(self.s_food, (unit.parts[0][0].x, unit.parts[0][0].y))
        if not self.game_started:
            score1 = self.score_font.render('%r' % (self.score[0]), True, self.score_color)
            score2 = self.score_font.render('%r' % (self.score[1]), True, self.score_color)
            self.display.blit(score1, (self.score_position[0] * 0.8, self.score_position[1]))
            self.display.blit(score2, (self.score_position[0] * 1.2, self.score_position[1]))
        if not self.training:
            pygame.display.update()

    def start(self):
        self.game_started = True
        self.new_life = True
        if not self.training: 
            clock = pygame.time.Clock()
        self.score = [0, 0]
        self.immunity = 0
        self.timer = self.default_timer
        while self.game_started or self.timer != 0:
            if not self.training:
                clock.tick_busy_loop(self.framerate)
            self.manage_events()
            if self.immunity != 0:
                self.immunity -= 1
            if self.timer != 0:
                self.timer = max(-1, self.timer - 1)
            else:
                self.new_life = False
                self.manage_input()
                self.manage_collisions()
            self.render()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('player1', type=str, default='HUMAN', help='TODO')
    parser.add_argument('player2', type=str, default='HUMAN', help='TODO')
    parser.add_argument('--training', action='store_true', help='TODO')
    parser.add_argument('--width', type=int, default=800, help='TODO')
    parser.add_argument('--height', type=int, default=800, help='TODO')
    parser.add_argument('--framerate', type=int, default=60, help='TODO')
    parser.add_argument('--font-path', type=str, default='games_aux/Bitter-Regular.otf')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--test-steps', type=int, default=10000, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=1000000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-path', type=str, default='memory.npy', help='TODO')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--video-save-freq', type=int, default=100000, help='TODO')
    parser.add_argument('--video-save-path', type=str, default='videos', help='TOOD')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memory', type=str, help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--log', type=str, help='TODO')
    args = parser.parse_args()
    args.image_shape = [None, 80, 80, 4]
    args.state_size = args.image_shape[-1]
    print(args)
    Snake(args)

if __name__ == '__main__':
    main()
