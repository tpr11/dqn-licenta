import argparse
import signal
import sys
import numpy as np

from Environment import ALEEnvironment
from DQN_EWC import DQN
from Memory import Memory
from Agent import epoch

def train_game(n, args, game, dqn, memory, memory_save_path, log, EWC_weights=None):
    args.n = n
    args.memory_save_path = memory_save_path
    args.model_save_path = None
    args.log = log
    env = ALEEnvironment(game, args.image_shape[1:3], args.state_size, args.frame_skip, args.render)
    if args.verbose:
        print('GAME %s: Beginning training for %d steps' % (game, args.train_steps))
    epoch(args.train_steps, env, dqn, memory, args.epsilon_train, args, training=True, EWC_weights=EWC_weights)
    if args.verbose:        
        print('GAME %s: Beginning testing for %d steps' % (game, args.test_steps))
    epoch(args.test_steps, env, dqn, memory, args.epsilon_test, args, training=False, EWC_weights=EWC_weights)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('games', nargs='+', help='TODO')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--train-epochs', nargs='+', type=int, help='TODO')
    parser.add_argument('--test-steps', type=int, default=0, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--EWC-wait', type=int, default=20000000, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=500000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--memory-fisher-samples', type=int, default=1024, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-paths', nargs='+', help='TODO')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--video-save-freq', type=int, default=100000, help='TODO')
    parser.add_argument('--video-save-path', type=str, default='videos', help='TODO')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memories', nargs='+', help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--testing', action='store_true', help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--render', action='store_true', help='TODO')
    parser.add_argument('--logs', nargs='+', help='TODO')
    args = parser.parse_args()

    args.model_save_path_ = args.model_save_path
    args.image_shape = [None, 84, 84, 4]
    args.state_size = args.image_shape[-1]
    args.epsilon_train = lambda i: max(0.01, 1 - 0.99 * i / 1000000)
    args.epsilon_test = lambda _: args.test_epsilon

    print(args)

    dqn = DQN(args.image_shape, 18, 2, [[[8,8,4,32], [1,4,4,1]], [[4,4,32,64], [1,2,2,1]], [[3,3,64,128], [1,1,1,1]], 1024])
    if args.model is not None:
        dqn.restore(args.model)
    
    if not args.testing:
        args.video_save_path = None

        def saver(*a):
            print('SIGINT. Saving...')
            dqn.save(args.model_save_path_, sum([memory.iteration for memory in memories]))
            for i, memory in enumerate(memories):
                memory.save(args.memory_save_paths[i])
            sys.exit(0)
        
        memories = []
        if args.memories:
            for memory in args.memories:
                if memory.lower() == 'none':
                    memories.append(Memory(args.memory_size, 18))
                else:
                    memories.append(Memory.load(memory))

        signal.signal(signal.SIGINT, saver)

        which = 0
        for i, memory in enumerate(memories):
            if memory.active:
                which = i
                break
        memories[which].active = True
        
        while sum([memory.iteration for memory in memories]) < args.EWC_wait:
            for _ in range(int(memories[which].iteration / args.train_steps) % args.train_epochs[which], args.train_epochs[which]):
                train_game(which, args, args.games[which], dqn, memories[which], args.memory_save_paths[which], args.logs[which])
                dqn.save(args.model_save_path_, sum([memory.iteration for memory in memories]))
            memories[which].active = False
            which = (which + 1) % len(args.games)
            memories[which].active = True

        if 'EWC_weights' not in memories[which].__dict__ or 'Fisher' not in memories[which].__dict__:
            memories[which].EWC_weights = dqn.sess.run(dqn.trainable_weights)
            memories[which].Fisher = dqn.compute_fisher((which- 1) % len(memories), memories[(which - 1) % len(memories)].sample(args.memory_fisher_samples)[0])
        dqn.update_fisher(memories[which].Fisher)
        
        while True:
            for _ in range(int(memories[which].iteration / args.train_steps) % args.train_epochs[which], args.train_epochs[which]):
                train_game(which, args, args.games[which], dqn, memories[which], args.memory_save_paths[which], args.logs[which], memories[which].EWC_weights)
                dqn.save(args.model_save_path_, sum([memory.iteration for memory in memories]))
            memories[which].active = False
            which = (which + 1) % len(args.games) 
            memories[which].active = True
            memories[which].EWC_weights = dqn.sess.run(dqn.trainable_weights)
            memories[which].Fisher = dqn.compute_fisher(which, memories[which].sample(args.memory_fisher_samples)[0]) 
            dqn.update_fisher(memories[which].Fisher)
    else:
        args.train_steps = 0
        memory = Memory(1, 18)
        while True:
            for i, game in enumerate(args.games):
                train_game(i, args, game, dqn, memory, None, None)


if __name__ == '__main__':
    main()
