import sys
import os
import sys
import random
import time
import argparse

import numpy as np
import tensorflow as tf
import scipy.ndimage
import pygame
import signal

from DQN import DQN
from Memory import Memory

class Unit:

    class Roles:
        PLAYER = 11
        PLAYER1 = 11
        PLAYER2 = 22
        BALL = 1

    def __init__(self, role, position, size, v_x, v_y, v_decay, v_x_max=None, v_y_max=None, lives=None, actions=None, ai=False):
        self.role = role
        self.x, self.y = position
        self.v_x_max, self.v_y_max = 999999, 999999
        self.v_x_min, self.v_y_min = 0, 0
        if role is Unit.Roles.BALL:
            self.v_x_max, self.v_y_max = v_x_max, v_y_max
            self.v_x_min, self.v_y_min = v_x, v_y
        self.v_x, self.v_y = v_x, v_y
        self.width, self.height = size
        self.v_decay = v_decay
        self.actions = actions
        self.ai = ai
        self.lives = lives
    def move(self):
        self.x = int(self.x + self.v_x)
        self.y = int(self.y + self.v_y)
    def decay_speed(self):
        self.v_x = np.sign(self.v_x) * min(self.v_x_max, max(self.v_x_min, np.abs(self.v_x * (1 - self.v_decay))))
        self.v_y = np.sign(self.v_y) * min(self.v_y_max, max(self.v_y_min, np.abs(self.v_y * (1 - self.v_decay))))

class Pong:

    class Events:
        DEATH = 0

    def saver(self, *args, **kwargs):
        print('SIGINT. Saving...')
        if self.args.model_save_path:
            self.model.save(self.args.model_save_path, self.memory.iteration)
        if self.args.memory_save_path: 
            self.memory.save(self.args.memory_save_path)
        sys.exit(0)

    def __init__(self, args):
        
        self.args = args

        self.width, self.height = args.width, args.height
        self.framerate = args.framerate
        self.training = self.args.player1 != 'HUMAN' and self.args.player2 != 'HUMAN'
        self.game_actions = ['NOTHING', 'UP', 'DOWN', 'LEFT', 'RIGHT']
        self.game_started = False

        self.background_color = np.array([0, 0, 0], dtype=np.uint8)

        self.player_shape = (self.width/80, self.height/10 , 3)
        self.player_speed = np.floor(0.013258252 * np.sqrt(self.width**2 + self.height**2))
        self.player_v_decay = 0.95
        self.player_lives = 10
        self.player1_color = np.array([255, 200, 0], dtype=np.uint8)
        self.player2_color = np.array([0, 200, 255], dtype=np.uint8)

        self.ball_shape = (self.height/80, self.height/80, 3)
        self.ball_speed = np.floor(0.006629126 * np.sqrt(self.width**2 + self.height**2))
        self.ball_v_decay = -0.001
        self.ball_color = np.array([255, 255, 255], dtype=np.uint8)

        pygame.init()
        self.display = pygame.display.set_mode((self.width, self.height))

        self.score_font = pygame.font.Font(args.font_path, self.height/16)
        self.score_color = (255, 255, 255)
        self.score1_position = (int(0.375 * self.width), int(0.0625 * self.height))
        self.score2_position = (int(0.59375 * self.width), int(0.0625 * self.height))
        
        self.s_background = pygame.surfarray.make_surface(np.ones((self.width, self.height, 3)) * self.background_color)
        self.s_player1 = pygame.surfarray.make_surface(np.ones(self.player_shape, dtype=np.uint8) * self.player1_color)
        self.s_player2 = pygame.surfarray.make_surface(np.ones(self.player_shape, dtype=np.uint8) * self.player2_color)
        self.s_ball = pygame.surfarray.make_surface(np.ones(self.ball_shape) * self.ball_color)

        self.default_timer = 30

        self.grayscale = self.grayscale = np.array([.299, .587, .114])
        self.ndzoom_shape = (1.0 * self.args.image_shape[1]/self.height, 1.0 * self.args.image_shape[2]/self.width)

        if self.training:
            self.epsilon = lambda i: max(0.1, 1 - 0.9 * i / 1000000) 
        else:
            self.epsilon = lambda _: self.args.test_epsilon

        self.init_units()

        signal.signal(signal.SIGINT, self.saver)

        if self.training:
            while True:
                starting_iteration = self.memory.iteration
                strs = ([], [])
                while self.memory.iteration - starting_iteration < self.args.train_steps:
                    iters = self.memory.iteration
                    self.player1.total_reward = 0 
                    self.player1.acting = 0
                    self.player1.s0 = None
                    self.player2.total_reward = 0 
                    self.player2.acting = 0
                    self.player2.s0 = None
                    t0 = time.time()
                    self.start()
                    t1 = time.time()
                    self.memory.episode += 1
                    strs[0].append('PLAYER 1    EP %d    IT %d (%5.2f%%)    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, 100.0 * (self.memory.iteration - starting_iteration) / self.args.train_steps, self.player1.total_reward, t1 - t0, (self.memory.iteration - iters) / (t1 - t0)))
                    strs[1].append('PLAYER 2    EP %d    IT %d (%5.2f%%)    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, 100.0 * (self.memory.iteration - starting_iteration) / self.args.train_steps, self.player2.total_reward, t1 - t0, (self.memory.iteration - iters) / (t1 - t0)))
                    if self.args.verbose:
                        print(strs[0][-1])
                        print(strs[1][-1])
                try:
                    if self.args.log:
                        with open(self.args.log, 'a') as f:
                            f.write('\n'.join(['%s\n%s' % (str0, str1) for (str0, str1) in zip(strs[0], strs[1])]) + '\n')
                except Exception as e:
                    print('Coulnd\'t save log: ' + e.message)
        else:
            self.start()

    @staticmethod
    def get_random_vectors(speed):
        d_x, d_y = (-1) ** np.random.randint(2), (-1) ** np.random.randint(2)
        v_y = d_y * np.random.randint(speed)
        v_x = d_x * np.floor(np.sqrt(speed**2 - v_y**2))
        return v_x, v_y

    def get_screen(self):
        return np.transpose(pygame.surfarray.array3d(self.display), axes=[1, 0, 2])

    def preprocess(self, img):
        frame = np.dot(img, self.grayscale).astype(np.uint8)
        frame = scipy.ndimage.zoom(frame, self.ndzoom_shape)
        frame = np.resize(frame, (self.args.image_shape[1], self.args.image_shape[2], 1))
        return frame

    def init_units(self):
        self.player1 = Unit(Unit.Roles.PLAYER1, (0, (self.height - self.player_shape[1]) / 2), (self.player_shape[0], self.player_shape[1]), 0, 0, v_decay=self.player_v_decay, lives=self.player_lives)
        self.player2 = Unit(Unit.Roles.PLAYER2, (self.width - self.player_shape[0], (self.height - self.player_shape[1]) / 2), (self.player_shape[0], self.player_shape[1]), 0, 0, v_decay=self.player_v_decay, lives=self.player_lives)
        self.ball = Unit(Unit.Roles.BALL, (self.width/2, self.height/2), (self.ball_shape[0], self.ball_shape[1]), *Pong.get_random_vectors(self.ball_speed), v_decay=self.ball_v_decay, v_x_max=3.0/160 * self.width, v_y_max=3.0/160 * self.height)
        self.units = [self.ball, self.player1, self.player2]
        if self.args.player1 != 'HUMAN' or self.args.player2 != 'HUMAN':
            self.render()
            self.state = []
            for _ in range(self.args.state_size):
                self.state.append(self.preprocess(self.get_screen()))
            if self.args.memory:
                self.memory = Memory.load(self.args.memory)
            else:
                self.memory = Memory(self.args.memory_size, len(self.game_actions))
            self.model = DQN(self.args.image_shape, len(self.game_actions))
            if self.args.model:
                self.model.restore(self.args.model)
        if self.args.player1 != 'HUMAN':
            self.player1.ai = True
            self.player1.total_reward = 0 
            self.player1.acting = 0
            self.player1.s0 = None
        else:
            self.player1.actions = {'UP': pygame.K_w, 'DOWN': pygame.K_s}
        if self.args.player2 != 'HUMAN':
            self.player2.ai = True
            self.player2.total_reward = 0 
            self.player2.acting = 0
            self.player2.s0 = None
        else:
            self.player2.actions = {'UP': pygame.K_UP, 'DOWN': pygame.K_DOWN}

    def manage_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.game_started = False
                self.timer = 0
            elif event.type == Pong.Events.DEATH:
                self.new_life = True
                self.timer = self.default_timer
                event.player.lives -= 1
                if event.player.role is Unit.Roles.PLAYER1:
                    self.score[1] += 1
                    if self.training:
                        self.player1.r -= 1
                        self.player2.r += 1
                        self.player1.acting = self.args.frame_skip
                        self.player2.acting = self.args.frame_skip
                elif event.player.role is Unit.Roles.PLAYER2:
                    self.score[0] += 1
                    if self.training:
                        self.player1.r += 1
                        self.player2.r -= 1
                        self.player1.acting = self.args.frame_skip
                        self.player2.acting = self.args.frame_skip
                self.player1.x, self.player1.y = (0, (self.height - self.player_shape[1]) / 2)
                self.player1.v_x, self.player1.v_y = 0, 0
                self.player2.x, self.player2.y = (self.width - self.player_shape[0], (self.height - self.player_shape[1]) / 2)
                self.player2_v_x, self.player2.v_y = 0, 0
                self.ball.x, self.ball.y = (self.width/2, self.height/2)
                self.ball.v_x, self.ball.v_y = Pong.get_random_vectors(self.ball_speed)
                if event.player.lives is 0:
                    self.game_started = False
                    if not self.training:
                        self.timer = -1
                    else:
                        self.timer = 0
                else:
                    self.render()
                    self.state =[]
                    for _ in range(self.args.state_size):
                        self.state.append(self.preprocess(self.get_screen()))

    def manage_input(self):
        keys = pygame.key.get_pressed()
        for unit in self.units:
            if unit.role % Unit.Roles.PLAYER is 0:
                if unit.ai:
                    if unit.acting == self.args.frame_skip:
                        if unit.lives < unit.previous_lives or not self.game_started:
                            unit.terminal = True
                        frame = np.maximum(unit.previous_frame, unit.current_frame)
                        if unit.s0 is None:
                            if unit is self.player1:
                                unit.s1 = self.state[:-1]
                            if unit is self.player2:
                                unit.s1 = [np.flip(s, axis=1) for s in self.state[:-1]]
                        else:    
                            unit.s1 = unit.s0[:-1]
                        unit.s1.insert(0, self.preprocess(frame))
                        if self.training and unit is self.player1 and unit.s0 is not None:
                            self.memory.add((unit.s0, unit.a, unit.r, unit.s1, unit.terminal))
                            if (self.args.memory_init is None or self.memory.iteration > self.args.memory_init) and \
                               self.args.train_wait is not None and self.memory.iteration % self.args.train_wait is 0:
                                self.model.train(self.memory.sample(self.args.batch_size))
                        if unit.terminal:
                            unit.s0 = None
                        else:
                            unit.s0 = unit.s1
                        if self.training and unit is self.player1 and (self.args.memory_init is None or self.memory.iteration > self.args.memory_init):
                            if self.args.model_save_path and self.memory.iteration % self.args.model_save_freq is 0:
                                if self.args.verbose: print('Saving model.')
                                self.model.save(self.args.model_save_path, self.memory.iteration)
                            if self.args.memory_save_path and self.memory.iteration % self.args.memory_save_freq is 0:
                                if self.args.verbose: print('Saving memory.')
                                self.memory.save(self.args.memory_save_path)
                            if self.args.model_update_freq is not None and self.memory.iteration % self.args.model_update_freq is 0:
                                if self.args.verbose: print('Updating target models.')
                                self.model.update_target()
                        unit.total_reward += unit.r
                        unit.acting = 0
                        if self.training and unit is self.player1:
                            self.memory.iteration += 1
                    elif unit.acting > 0:
                        unit.previous_frame = unit.current_frame
                        if unit is self.player1:
                            unit.current_frame = self.get_screen()
                        if unit is self.player2:
                            unit.current_frame = np.flip(self.get_screen(), axis=1)
                        if self.game_actions[unit.a] is 'UP':
                            unit.v_y -= self.player_speed
                        if self.game_actions[unit.a] is 'DOWN':
                            unit.v_y += self.player_speed
                        unit.acting += 1
                    if unit.acting == 0:
                        if unit.s0 is None or random.random() < self.epsilon(self.memory.iteration):
                            unit.a = random.randrange(len(self.game_actions))
                        else:
                            s = np.reshape(np.concatenate(unit.s0, axis=2), (1, self.args.image_shape[1], self.args.image_shape[2], self.args.state_size))
                            unit.a = np.argmax(self.model.predict(s), axis=1)[0]
                        unit.previous_lives = unit.lives
                        unit.r = 0
                        unit.terminal = False
                        if unit is self.player1:
                            unit.previous_frame = self.get_screen() 
                        if unit is self.player2:
                            unit.previous_frame = np.flip( self.get_screen() , axis=1)
                        unit.current_frame = unit.previous_frame
                        if self.game_actions[unit.a] is 'UP':
                            unit.v_y -= self.player_speed
                        if self.game_actions[unit.a] is 'DOWN':
                            unit.v_y += self.player_speed
                        unit.acting += 1
                else:
                    if keys[unit.actions['UP']]:
                        unit.v_y -= self.player_speed
                    if keys[unit.actions['DOWN']]:
                        unit.v_y += self.player_speed

    def manage_collisions(self):
        for unit in self.units:
            unit.move()
        for unit in self.units:
            if unit.role is Unit.Roles.BALL:
                if unit.y < 0:
                    unit.y = 0
                    unit.v_y *= -1
                elif unit.y >= self.height - unit.height:
                    unit.y = self.height - unit.height
                    unit.v_y *= -1
                elif unit.x < 0:
                    if unit.x - unit.v_x >= self.player1.x + self.player1.width and \
                       (self.player1.y <= unit.y + unit.height <= self.player1.y + self.player1.height or \
                        self.player1.y <= unit.y <= self.player1.y + self.player1.height):
                        unit.v_x *= -1
                        unit.v_y += self.player1.v_y
                        unit.x = self.player1.x + self.player1.width
                    else:
                        pygame.event.post(pygame.event.Event(Pong.Events.DEATH, player=self.player1))
                elif unit.x + unit.width >= self.width:
                    if unit.x + unit.width - unit.v_x <= self.player2.x and \
                       (self.player2.y <= unit.y + unit.height <= self.player2.y + self.player2.height or \
                        self.player2.y <= unit.y <= self.player2.y + self.player2.height):
                        unit.v_x *= -1
                        unit.v_y += self.player2.v_y
                        unit.x = self.player2.x - unit.width
                    else:
                        pygame.event.post(pygame.event.Event(Pong.Events.DEATH, player=self.player2))
                else:
                    for other_unit in self.units:
                        if other_unit is not unit and \
                           (0 <= other_unit.y + other_unit.height - unit.y <= other_unit.height or \
                            0 <= other_unit.y + other_unit.height - unit.y - unit.height <= other_unit.height):
                            if ((other_unit.role is Unit.Roles.PLAYER1 and \
                                unit.x <= other_unit.x + other_unit.width) or \
                                (other_unit.role is Unit.Roles.PLAYER2 and \
                                other_unit.x <= unit.x + unit.width)):
                                unit.v_x *= -1
                                unit.v_y += other_unit.v_y
                                if other_unit.role is Unit.Roles.PLAYER1 and \
                                   unit.x < other_unit.x + other_unit.width:
                                    unit.x = other_unit.x + other_unit.width
                                if other_unit.role is Unit.Roles.PLAYER2 and \
                                   other_unit.x < unit.x + unit.width:
                                    unit.x = other_unit.x - unit.width
            else:
                if unit.y < 0:
                    unit.y = 0
                    unit.v_y = 0
                if unit.y >= self.height - unit.height:
                    unit.y = self.height - unit.height
                    unit.v_y = 0
        for unit in self.units:
            unit.decay_speed()

    def render(self):
        self.display.blit(self.s_background, (0,0))
        self.display.blit(self.s_ball, (self.ball.x, self.ball.y))
        self.display.blit(self.s_player1, (self.player1.x, self.player1.y))
        self.display.blit(self.s_player2, (self.player2.x, self.player2.y))
        if not self.game_started:
            s_winner = self.score_font.render('Player %d WINS' % (1 if self.player2.lives is 0 else 2), True, self.score_color)
            self.display.blit(s_winner, ((self.width - s_winner.get_width()) / 2, int(0.0625 * self.height)))
        elif self.new_life:
            score1 = self.score_font.render('%r' % (self.score[0]), True, self.score_color, tuple(self.background_color))
            score2 = self.score_font.render('%r' % (self.score[1]), True, self.score_color, tuple(self.background_color))
            self.display.blit(score1, self.score1_position)
            self.display.blit(score2, self.score2_position)
        pygame.display.update()

    def start(self):
        self.player1.lives = self.player_lives
        self.player2.lives = self.player_lives
        self.game_started = True
        self.new_life = True
        if not self.training: 
            clock = pygame.time.Clock()
        self.score = [0, 0]
        self.timer = self.default_timer
        while self.game_started or self.timer != 0:
            if not self.training: 
                clock.tick_busy_loop(self.framerate)
            self.manage_events()
            if self.timer != 0:
                self.timer = max(-1, self.timer - 1)
            else:
                self.new_life = False
                self.manage_input()
                self.manage_collisions()
            self.render()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('player1', type=str, default='HUMAN', help='TODO')
    parser.add_argument('player2', type=str, default='HUMAN', help='TODO')
    parser.add_argument('--width', type=int, default=800, help='TODO')
    parser.add_argument('--height', type=int, default=800, help='TODO')
    parser.add_argument('--framerate', type=int, default=60, help='TODO')
    parser.add_argument('--font-path', type=str, default='games_aux/Bitter-Regular.otf')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--test-steps', type=int, default=10000, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=1000000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-path', type=str, default='memory.npy', help='TODO')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--video-save-freq', type=int, default=100000, help='TODO')
    parser.add_argument('--video-save-path', type=str, default='videos', help='TOOD')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memory', type=str, help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--log', type=str, help='TODO')
    args = parser.parse_args()
    args.image_shape = [None, 80, 80, 4]
    args.state_size = args.image_shape[-1]
    print(args)
    Pong(args)

if __name__ == '__main__':
    main()