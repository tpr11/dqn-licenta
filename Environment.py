from ale_python_interface import ALEInterface
import scipy.ndimage
import numpy as np

class ALEEnvironment:

    def __init__(self, rom, image_shape, state_size, frame_skip, render, full_action_set=False):
        self.env = ALEInterface()
        self.env.setInt(b'random_seed', 123456)
        self.env.setFloat(b'repeat_action_probability', 0.0)
        self.env.setBool(b'display_screen', render)
        self.env.loadROM(rom)
        self.image_shape = image_shape
        self.state_size = state_size
        self.grayscale = np.array([.299, .587, .114])
        self.env.reset_game()
        self.ndzoom_shape = (1.0 * self.image_shape[0]/self.env.getScreenRGB().shape[0], 1.0 * self.image_shape[1]/self.env.getScreenRGB().shape[1])
        self.frame_skip = frame_skip
        self.action_set = self.env.getMinimalActionSet() if not full_action_set else self.env.getLegalActionSet()
        self.num_actions = len(self.action_set)
        self.reset()

    def preprocess(self, img):
        frame = np.dot(img, self.grayscale).astype(np.uint8)
        frame = scipy.ndimage.zoom(frame, self.ndzoom_shape)
        frame = np.resize(frame, (self.image_shape[0], self.image_shape[1], 1))
        return frame

    def act(self, action):
        lives = self.env.lives()
        r = 0
        terminal = False
        for _ in range(self.frame_skip):
            previous_frame = self.env.getScreenRGB()
            r += self.env.act(self.action_set[action])
            frame = self.env.getScreenRGB()
            if self.env.lives() < lives or self.env.game_over():
                terminal = True
                break
        frame = np.maximum(previous_frame, frame)
        self.state = self.state[:3]
        self.state.insert(0, self.preprocess(frame))
        return self.state, r, terminal

    def game_over(self):
        return self.env.game_over()

    def reset(self):
        self.env.reset_game()
        self.state = []
        for _ in range(self.state_size):
            self.state.append(self.preprocess(self.env.getScreenRGB()))
        return self.state