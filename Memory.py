import numpy as np
import random
from collections import namedtuple

class Memory:

    def __init__(self, size=None, num_actions=None):
        if size is None or num_actions is None:
            return
        self.memory = []
        self.p = np.zeros(size)
        self.alpha = 0.6
        self.beta = 0.4
        self.size = size
        self.num_actions = num_actions
        self.iteration = 0
        self.episode = 0
        self.memi = 0
        self.full = False
        
    def add(self, sample):
        if self.full:
            self.memory[self.memi] = sample
            self.p[self.memi] = np.max(self.p)
            self.memi = (self.memi + 1) % self.size
        else:
            self.memory.append(sample)
            self.p[len(self.memory) % self.size - 1] = np.max(self.p) if len(self.memory) > 1 else 1
            if len(self.memory) == self.size:
                self.full = True
        
    def sample(self, num):
        batch = random.sample(self.memory, num)
        input_shape = batch[0][0][0].shape
        state_size = len(batch[0][0])
        start_states = np.empty((len(batch), input_shape[0], input_shape[1], state_size), dtype=np.uint8)
        actions = np.zeros((len(batch), self.num_actions), dtype=np.int32)
        rewards = np.empty((len(batch),), dtype=np.int32)
        next_states = np.empty((len(batch), input_shape[0], input_shape[1], state_size), dtype=np.uint8)
        terminal = np.empty((len(batch),), dtype=np.bool)
        for i in range(len(batch)):
            start_states[i,:,:,:] = np.concatenate(batch[i][0], axis=2)
            actions[i,batch[i][1]] = 1
            rewards[i] = batch[i][2]
            next_states[i,:,:,:] = np.concatenate(batch[i][3], axis=2)
            terminal[i] = batch[i][4]
        return start_states, actions, rewards, next_states, terminal

    def sample_prioritized(self, num):
        idx = np.random.choice(np.arange(len(self.memory)), num, p=(self.p**self.alpha/np.sum(self.p**self.alpha))[:len(self.memory)])
        w = (self.size * self.p[idx]) ** (-self.beta)
        w /= np.max(w)
        input_shape = self.memory[0][0][0].shape
        state_size = len(self.memory[0][0])
        start_states = np.empty((num, input_shape[0], input_shape[1], state_size), dtype=np.uint8)
        actions = np.zeros((num, self.num_actions), dtype=np.int32)
        rewards = np.empty((num,), dtype=np.int32)
        next_states = np.empty((num, input_shape[0], input_shape[1], state_size), dtype=np.uint8)
        terminal = np.empty((num,), dtype=np.bool)
        for i in range(num):
            start_states[i,:,:,:] = np.concatenate(self.memory[idx[i]][0], axis=2)
            actions[i, self.memory[idx[i]][1]] = 1
            rewards[i] = self.memory[idx[i]][2]
            next_states[i,:,:,:] = np.concatenate(self.memory[idx[i]][3], axis=2)
            terminal[i] = self.memory[idx[i]][4]
        return start_states, actions, rewards, next_states, terminal, w, idx

    def update_priorities(self, idx, delta):
        self.p[idx] = np.abs(delta)

    def save(self, path):
        np.save(path, self.__dict__)
        
    @staticmethod
    def load(path):
        memory = Memory()
        memory.__dict__ = np.load(path).item()
        return memory