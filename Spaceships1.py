import sys
import os
import sys
import random
import time
import argparse
import copy
import signal

import numpy as np
import scipy.ndimage
import pygame

from sprite_strip_anim import SpriteStripAnim
from DQN import DQN
from Memory import Memory

class Point:
    def __init__(self, x, y):
        self.x, self.y = x, y
    def __neg__(self):
        return Point(-self.x, -self.y)
    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.x + other[0], self.y + other[1])
        raise NotImplementedError('+ operator for Point requires either Point or (x, y) tuple.')
    def __sub__(self, other):
        if isinstance(other, Point):
            return Point(self.x - other.x, self.y - other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.x - other[0], self.y - other[1])
        raise NotImplementedError('- operator for Point requires either Point or (x, y) tuple.')
    def __mul__(self, other):
        if isinstance(other, Point):
            return Point(self.x * other.x, self.y * other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.y * other[0], self.y * other[0])
        elif isinstance(other, (int, float, long, complex)):
            return Point(self.x * other, self.y * other)
        raise NotImplementedError('* operator for Point requires either Point, (x, y) tuple or number.')
    def __div__(self, s):
        return Point(1.0 * self.x / s, 1.0 * self.y / s)
    def __abs__(self):
        return Point(np.abs(self.x), np.abs(self.y))
    def __iter__(self):
        yield self.x
        yield self.y
    def __repr__(self):
        return '(%r, %r)' % (self.x, self.y)

class Unit():

    DEFAULT_PLAYER_SPEED = 3
    DEFAULT_PROJECTILE_SPEED = 15
    DEFAULT_HP = 1000
    DEFAULT_PROJECTILE_DAMAGE = 100

    class Roles:
        PLAYER = 11
        PLAYER1 = 11
        PLAYER2 = 22
        WALL = 13
        PROJECTILE = 17

    class Keys:
        LEFT = 0
        RIGHT = 1
        FIRE = 2

    def __init__(self, role, center, size, speed, anim, keys=None, static=False):
        self.role = role
        self.center = center
        self.size = size
        self.speed = speed
        self.anim = anim
        self.keys = keys
        self.static = static
        self.damage = 0
        if self.role % Unit.Roles.PROJECTILE == 0:
            self.damage = Unit.DEFAULT_PROJECTILE_DAMAGE
        self.fired = 0
        self.hp = Unit.DEFAULT_HP

    def alive(self):
        return self.hp > 0

    def get_collision_box(self):
        return [self.center - self.size / 2.0, self.center + self.size / 2.0]

class SpaceshipsAI:

    class PlayerAuxiliary:
        def __init__(self, control, s0=None, a=-1, r=0, s1=None, terminal=False, frame=None, total_reward=0, acting=0, active=False):
            self.control = control
            self.s0 = s0
            self.a = a
            self.r = r
            self.s1 = s1
            self.terminal = terminal
            self.total_reward = total_reward
            self.acting = acting
            self.active = active
            self.random_frames = 0
        def __repr__(self):
            return '%r' % (self.__dict__)

    def saver(self, *args, **kwargs):
        print('SIGINT. Saving...')
        if self.args.model_save_path:
            self.model.save(self.args.model_save_path, self.memory.iteration)
        if self.args.memory_save_path: 
            self.memory.save(self.args.memory_save_path)
        sys.exit(0)

    def __init__(self, spaceships, args):
        self.spaceships = spaceships
        self.args = args
        
        self.action_space = [Unit.Keys.FIRE, Unit.Keys.LEFT, Unit.Keys.RIGHT, -1]
        self.model = DQN(self.args.image_shape, len(self.action_space))
        if self.args.model != None:
            self.model.restore(self.args.model)

        if self.args.memory != None:
            self.memory = Memory.load(self.args.memory)
        else:
            self.memory = Memory(self.args.memory_size, len(self.action_space))
        self.memory.player1_active = True
            
        signal.signal(signal.SIGINT, self.saver)

        if self.args.training:
            self.epsilon = lambda i: max(0.1, 1 - 0.90 * i / 1000000)
        else:
            self.epsilon = lambda _: self.args.test_epsilon
        self.grayscale = np.array([.299, .587, .114])
        self.ndzoom_shape = (1.0 * self.args.image_shape[1] / self.spaceships.height, 1.0 * self.args.image_shape[2] / self.spaceships.width)
        self.strs = []
        self.iters = self.memory.iteration

    def _preprocess(self, img):
        frame = np.dot(img, self.grayscale).astype(np.uint8)
        frame = scipy.ndimage.zoom(frame, self.ndzoom_shape)
        frame = np.resize(frame, (self.args.image_shape[1], self.args.image_shape[2], 1))
        return frame

    def _get_player(self, role):
        return self.player1 if role == Unit.Roles.PLAYER1 else self.player2

    def is_ai(self, player):
        return self.player1.control != 'human' if player.role == Unit.Roles.PLAYER1 else self.player2.control != 'human'

    def notify_game_start(self, screen):
        self.state = []
        for _ in range(self.args.state_size):
            self.state.append(self._preprocess(screen))
        self.player1 = SpaceshipsAI.PlayerAuxiliary(self.args.player1, active=self.memory.player1_active)
        self.player2 = SpaceshipsAI.PlayerAuxiliary(self.args.player2, active=(not self.memory.player1_active))
        self.starting_iteration = self.memory.iteration
        self.t0 = time.time()

    def get_action(self, role, screen):
        player = self._get_player(role)
        if player.control == 'human':
            return None
        elif player.control == 'dqn':
            if player.acting == self.args.frame_skip:
                if player.s0 == None:
                    player.s1 = self.state[:-1]
                else:
                    player.s1 = player.s0[:-1]
                player.s1.insert(0, self._preprocess(player.frame))
                if self.args.training and player.active and player.s0 != None:
                    self.memory.add((player.s0, player.a, player.r, player.s1, player.terminal))
                    if (self.args.memory_init != None and self.memory.iteration > self.args.memory_init) and \
                       self.args.train_wait != None and self.memory.iteration % self.args.train_wait == 0:
                        batch = self.memory.sample(self.args.batch_size)
                        self.model.train(batch)
                if player.terminal:
                    player.s0 = None
                else:
                    player.s0 = player.s1
                if self.args.training and player.active and \
                   (self.args.memory_init is None or self.memory.iteration > self.args.memory_init):
                    if self.args.model_save_path and self.memory.iteration % self.args.model_save_freq == 0:
                        if self.args.verbose: print('Saving model.')
                        self.model.save(self.args.model_save_path, self.memory.iteration)
                    if self.args.memory_save_path and self.memory.iteration % self.args.memory_save_freq == 0:
                        if self.args.verbose: print('Saving memory.')
                        self.memory.save(self.args.memory_save_path)
                    if self.args.model_update_freq != None and self.memory.iteration % self.args.model_update_freq == 0:
                        if self.args.verbose: print('Updating target models.')
                        self.model.update_target()
                player.total_reward += player.r
                player.acting = 0
                if self.args.training and player.active:
                    self.memory.iteration += 1
            elif player.acting > 0:
                player.frame = screen
                player.acting += 1
            if player.acting == 0:
                if player.s0 == None or random.random() < self.epsilon(self.memory.iteration):
                    player.a = random.randrange(len(self.action_space))
                else:
                    s = np.reshape(np.concatenate(player.s0, axis=2), (1, self.args.image_shape[1], self.args.image_shape[2], self.args.state_size))
                    player.a = np.argmax(self.model.predict(s), axis=1)[0]
                player.frame = screen
                player.r = 0
                player.terminal = False
                player.acting += 1
        elif player.control == 'deterministic':
            if player.random_frames > 0:
                player.random_frames -= 1
            else:
                center, other_center = (self.spaceships.player1.center, self.spaceships.player2.center) if role == Unit.Roles.PLAYER1 else \
                                    (self.spaceships.player2.center, self.spaceships.player1.center)
                hitbox = (self.spaceships.player1 if role == Unit.Roles.PLAYER1 else self.spaceships.player2).get_collision_box()
                left_hitbox = (hitbox[0] - (self.spaceships.ship_speed, 0), hitbox[1] - (self.spaceships.ship_speed, 0))
                right_hitbox = (hitbox[0] + (self.spaceships.ship_speed, 0), hitbox[1] + (self.spaceships.ship_speed, 0))
                left_hit = hit = right_hit = False
                for projectile in self.spaceships.projectiles:
                    pb = projectile.get_collision_box()
                    pb[0] += (0, (-1 if role == Unit.Roles.PLAYER1 else 1) * 10 * self.spaceships.projectile_speed)
                    pb[1] += (0, (-1 if role == Unit.Roles.PLAYER1 else 1) * self.spaceships.projectile_speed)
                    if not left_hit and Spaceships.rect_overlap(left_hitbox, pb):
                        left_hit = True
                    if not hit and Spaceships.rect_overlap(hitbox, pb):
                        hit = True
                    if not right_hit and Spaceships.rect_overlap(right_hitbox, pb):
                        right_hit = True
                player.a = -1
                if -self.spaceships.projectile_size.x <= other_center.x - center.x <= self.spaceships.projectile_size.x:
                    player.a = 0
                elif other_center.x - center.x >= 0:
                    if right_hit:
                        player.a = 0
                    else:
                        player.a = 2
                else:
                    if left_hit:
                        player.a = 0
                    else:
                        player.a = 1
                if player.a == 0:
                    if hit:
                        if left_hit and right_hit:
                            player.a = random.randint(1, 2)
                            player.random_frames = self.spaceships.framerate / 5
                        elif left_hit:
                            player.a = 2
                        else:
                            player.a = 1
        return self.action_space[player.a]

    def notify_reward(self, role, reward):
        player = self._get_player(role)
        if player.control == 'human':
            return None
        player.r += reward

    def notify_terminal(self, screen):
        self.player1.terminal = True
        self.player1.frame = screen
        self.player1.acting = self.args.frame_skip
        self.player2.terminal = True
        self.player2.frame = screen
        self.player2.acting = self.args.frame_skip
        self.get_action(Unit.Roles.PLAYER1, screen)
        self.get_action(Unit.Roles.PLAYER2, screen)

    def notify_game_end(self):
        self.t1 = time.time()
        self.memory.episode += 1
        #self.memory.player1_active = not self.memory.player1_active
        self.strs.append('PLAYER 1    EP %d    IT %d    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, self.player1.total_reward, self.t1 - self.t0, (1.0 * self.memory.iteration - self.starting_iteration) / (self.t1 - self.t0)))
        self.strs.append('PLAYER 2    EP %d    IT %d    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, self.player2.total_reward, self.t1 - self.t0, (1.0 * self.memory.iteration - self.starting_iteration) / (self.t1 - self.t0)))
        if self.args.verbose:
            print self.strs[-2]
            print self.strs[-1]
        if self.args.train_steps != None and \
           self.memory.iteration - self.iters > self.args.train_steps:
            self.iters = self.memory.iteration
            if self.args.log:
                try: 
                    with open(self.args.log, 'a') as f:
                        f.write('\n'.join(self.strs))
                except Exception as e:
                    print 'Couldn\'t save log: ' + e.message
            self.strs = []

class Spaceships:

    class Events:
        BASE = pygame.USEREVENT
        PLAYER_DEATH = BASE + 1
        PLAYERS_DEATH = BASE + 2
        PLAYER_FIRE = BASE + 3
        PLAYER_HIT = BASE + 4
        PROJECTILE_DEATH = BASE + 5

    class States:
        LOADING = 0
        PLAYING = 1
        PAUSED = 2
        FINISHED = 3
        QUIT = 4

    def __init__(self, args):

        self.args = args

        if self.args.resolution == 'medium':
            self.width, self.height = 600, 400 
        elif self.args.resolution == 'small':
            self.width, self.height = 300, 200
        self.score_width, self.score_height = self.width, int(0.05 * self.height)
        self.framerate = self.args.framerate

        pygame.init()
        self.display = pygame.display.set_mode((self.width, self.height + self.score_height), pygame.FULLSCREEN if args.fullscreen else 0)
        self.score_screen = pygame.Surface((self.score_width, self.score_height))
        self.game_screen = pygame.Surface((self.width, self.height))

        if self.args.resolution == 'medium':
            self._load_sprites('games_aux/medium', 5, (16, 24), (5, 13), (16, 24), 18)
            self.ship_speed = 4
            self.projectile_speed = 12
        elif self.args.resolution == 'small':
            self._load_sprites('games_aux/small', 5, (8, 12), (3, 6), (8, 12), 9)
            self.ship_speed = 2
            self.projectile_speed = 6
        self._load_ai()

    def _load_sprites(self, base_dir, frames, ship_size, projectile_size, explosion_size, font_size):
        background = pygame.image.load(base_dir + '/background.png').convert()
        self.background = pygame.Surface((self.width, self.height)).convert()
        for i in range(0, self.width, background.get_size()[0]):
            for j in range(0, self.height, background.get_size()[1]):
                self.background.blit(background, (i, j))

        self.ship1_active_anim = SpriteStripAnim(base_dir + '/ship1_active.png', (0, 0, ship_size[0], ship_size[1]), 5, loop=True, frames=frames)
        for i, image in enumerate(SpriteStripAnim(base_dir + '/ship1_active.png', (0, ship_size[1], ship_size[0], ship_size[1]), 5, loop=True, frames=frames).images):
            self.ship1_active_anim.images.insert(i * 2 + 1, image)
        for i in range(len(self.ship1_active_anim.images)):
            self.ship1_active_anim.images[i] = pygame.transform.rotate(self.ship1_active_anim.images[i], 180)

        self.ship2_active_anim = SpriteStripAnim(base_dir + '/ship2_active.png', (0, 0, ship_size[0], ship_size[1]), 5, loop=True, frames=frames)
        for i, image in enumerate(SpriteStripAnim(base_dir + '/ship2_active.png', (0, ship_size[1], ship_size[0], ship_size[1]), 5, loop=True, frames=frames).images):
            self.ship2_active_anim.images.insert(i * 2 + 1, image)     

        self.player1_projectile_anim = SpriteStripAnim(base_dir + '/laser-bolts.png', (0, 0, projectile_size[0], projectile_size[1]), 2, loop=True, frames=frames*5)
        for i in range(len(self.player1_projectile_anim.images)):
            self.player1_projectile_anim.images[i] = (pygame.transform.rotate(self.player1_projectile_anim.images[i], 180))
        self.player2_projectile_anim = SpriteStripAnim(base_dir + '/laser-bolts.png', (0, 0, projectile_size[0], projectile_size[1]), 2, loop=True, frames=frames*5)

        self.explosion1_anim = SpriteStripAnim(base_dir + '/explosion.png', (0, 0, explosion_size[0], explosion_size[1]), 5, loop=False, frames=frames)
        for i in range(len(self.explosion1_anim.images)):
            self.explosion1_anim.images[i] = (pygame.transform.rotate(self.explosion1_anim.images[i], 180))
        self.explosion2_anim = SpriteStripAnim(base_dir + '/explosion.png', (0, 0, explosion_size[0], explosion_size[1]), 5, loop=False, frames=frames)

        self.ship_size = Point(*ship_size)
        self.projectile_size = Point(*projectile_size)
        self.explosion_size = Point(*explosion_size) 

        self.font = pygame.font.Font(base_dir + '/PixelFont.ttf', font_size)
        self.player1_tag = self.font.render('  RED', True, (255, 255, 255))
        self.player1_tag_pos = (0.05 * self.score_width, (self.score_height - self.player1_tag.get_size()[1]) / 2)
        self.player2_tag = self.font.render('GREEN', True, (255, 255, 255))
        self.player2_tag_pos = (0.95 * self.score_width - self.player2_tag.get_size()[0], (self.score_height - self.player2_tag.get_size()[1]) / 2)

        self.player1_hp_pos = (self.player1_tag_pos[0] + self.player1_tag.get_size()[0] + 10, self.player1_tag_pos[1])
        self.player2_hp_pos = (self.player2_tag_pos[0] - 10, self.player2_tag_pos[1])
        self.hp_full_length = 0.425 * (self.score_width - self.player1_tag.get_size()[0] - self.player2_tag.get_size()[0] - 20)
        self.hp_full_height = 0.75 * self.player1_tag.get_size()[1]

    def _load_ai(self):
        self.AI = SpaceshipsAI(self, self.args)

    def _init_sprites(self):
        self.ship1_active_anim.iter()
        self.ship2_active_anim.iter()
        self.player1_projectile_anim.iter()
        self.player2_projectile_anim.iter()
        self.explosion1_anim.iter()
        self.explosion2_anim.iter()

    def _init_units(self):
        self.player1 = Unit(
            Unit.Roles.PLAYER1, 
            Point(0.25 * self.width, 0.05 * self.height),
            self.ship_size,
            self.ship_speed,
            anim=self.ship1_active_anim, 
            keys={Unit.Keys.LEFT: pygame.K_a, Unit.Keys.RIGHT: pygame.K_d, Unit.Keys.FIRE: pygame.K_s})
        self.player2 = Unit(
            Unit.Roles.PLAYER2, 
            Point(0.75 * self.width, 0.95 * self.height),
            self.ship_size,
            self.ship_speed,
            anim=self.ship2_active_anim, 
            keys={Unit.Keys.LEFT: pygame.K_LEFT, Unit.Keys.RIGHT: pygame.K_RIGHT, Unit.Keys.FIRE: pygame.K_UP})
        self.players = {
            Unit.Roles.PLAYER1: self.player1,
            Unit.Roles.PLAYER2: self.player2
        }
        self.walls_aux_offset = 25
        self.projectiles = []
        self.explosions = []

    def _init_game(self):
        self.clock = pygame.time.Clock()
        self.state = Spaceships.States.LOADING
        self.score = {Unit.Roles.PLAYER1: 0, Unit.Roles.PLAYER2: 0}
        self.frames = 0

    def _finish_game(self):
        self.state = Spaceships.States.FINISHED

    def _manage_events(self):
        if self.frames >= 30 * self.framerate:
            self.player1.hp -= 1
            self.player2.hp -= 1
            self.AI.notify_reward(Unit.Roles.PLAYER1, -1)
            self.AI.notify_reward(Unit.Roles.PLAYER2, -1)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.state = Spaceships.States.QUIT
            elif event.type == Spaceships.Events.PLAYER_DEATH:
                self._finish_game()
                self.AI.notify_terminal(self.get_screen())
            elif event.type == pygame.KEYUP:
                if event.key == self.player1.keys[Unit.Keys.FIRE]:
                    self.player1.fired = 0
                    self.player1.speed = self.ship_speed
                if event.key == self.player2.keys[Unit.Keys.FIRE]:
                    self.player2.fired = 0
                    self.player2.speed = self.ship_speed

    def _manage_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
            pygame.event.post(pygame.event.Event(pygame.QUIT))
        for player in self.players.itervalues():
            if not player.alive():
                pygame.event.post(pygame.event.Event(Spaceships.Events.PLAYER_DEATH, player=player))
                continue
            action = None
            is_ai = self.AI.is_ai(player)
            action = self.AI.get_action(player.role, self.get_screen())
            moved = False
            if (not is_ai and keys[player.keys[Unit.Keys.LEFT]]) or \
               action == Unit.Keys.LEFT:
                player.center -= (player.speed, 0)
                player.speed = self.ship_speed
                moved = True
            if (not is_ai and keys[player.keys[Unit.Keys.RIGHT]]) or \
               action == Unit.Keys.RIGHT:
                player.center += (player.speed, 0)
                player.speed = self.ship_speed
                moved = True
            if not moved:
                player.hp -= 1
                self.AI.notify_reward(player.role, -1)
            if (not is_ai and keys[player.keys[Unit.Keys.FIRE]]) or \
               action == Unit.Keys.FIRE:
                player.speed = self.ship_speed / 2.0
                if player.fired > 0:
                    player.fired -= 1
                else:
                    player.fired = self.framerate / 5
                    if player.role == Unit.Roles.PLAYER1:
                        projectile = Unit(Unit.Roles.PROJECTILE, player.center, self.projectile_size, self.projectile_speed, self.player1_projectile_anim)
                        projectile.direction = 1
                    else:
                        projectile = Unit(Unit.Roles.PROJECTILE, player.center, self.projectile_size, self.projectile_speed, self.player2_projectile_anim)
                        projectile.direction = 0
                    projectile.originator = player
                    projectile.order = len(self.projectiles)
                    self.projectiles.append(projectile)

    @staticmethod
    def rect_overlap((A1, A2), (B1, B2)):
        return A1.x < B2.x and A2.x > B1.x and \
               A1.y < B2.y and A2.y > B1.y 

    def _manage_collisions(self):
        for projectile in self.projectiles:
            if projectile.direction == 0:
                projectile.center -= (0, projectile.speed)
            else:
                projectile.center += (0, projectile.speed)
        for player in self.players.itervalues():
            cb = player.get_collision_box()
            if player.center.x <= player.size.x / 2.0:
                player.center.x = player.size.x / 2.0
            if player.center.x >= self.width - player.size.x / 2.0:
                player.center.x = self.width - player.size.x / 2.0
        for i in xrange(len(self.projectiles) - 1, -1, -1):
            try:
                pb = self.projectiles[i].get_collision_box()
                if self.projectiles[i].center.y <= -self.walls_aux_offset or \
                self.projectiles[i].center.y >= self.height + self.walls_aux_offset:
                    del self.projectiles[i]
                for player in self.players.itervalues():
                    cb = player.get_collision_box()
                    if self.projectiles[i].originator != player and \
                    Spaceships.rect_overlap(cb, pb):
                        explosion = (self.explosion1_anim if player.role == Unit.Roles.PLAYER1 else self.explosion2_anim, player.center)
                        explosion[0].iter()
                        self.explosions.append(explosion)
                        player.hp -= self.projectiles[i].damage
                        self.AI.notify_reward(player.role, -self.projectiles[i].damage)
                        self.AI.notify_reward((1 + ((player.role / Unit.Roles.PLAYER) % 2)) * Unit.Roles.PLAYER, self.projectiles[i].damage)
                        del self.projectiles[i]
            except IndexError:
                pass

    def _blit_unit(self, surface, unit):
        diff = unit.center - unit.size / 2.0
        surface.blit(unit.anim.next(), (diff.x, diff.y))

    def get_screen(self):
        return np.transpose(pygame.surfarray.array3d(self.display), axes=[1, 0, 2])

    def _render(self):
        # self.game_screen.blit(self.background, (0, 0))
        self.game_screen.fill((0,0,0))
        for projectile in self.projectiles:
            self._blit_unit(self.game_screen, projectile)
        if self.player1.alive(): 
            self._blit_unit(self.game_screen, self.player1)
        if self.player2.alive(): 
            self._blit_unit(self.game_screen, self.player2)    
        for i in xrange(len(self.explosions) - 1, -1, -1):
            try: 
                diff = self.explosions[i][1] - self.explosion_size / 2.0
                self.game_screen.blit(self.explosions[i][0].next(), (diff.x, diff.y))
            except StopIteration:
                del self.explosions[i]
        if self.args.debug:
            self._render_debug()
        
        self.score_screen.fill((0,0,0))
        self.score_screen.blit(self.player1_tag, self.player1_tag_pos)
        pygame.draw.rect(self.score_screen, (0, 200, 0), [self.player1_hp_pos[0], self.player1_hp_pos[1], 1.0 * self.player1.hp / Unit.DEFAULT_HP * self.hp_full_length, self.hp_full_height])
        self.score_screen.blit(self.player2_tag, self.player2_tag_pos)
        pygame.draw.rect(self.score_screen, (0, 200, 0), [self.player2_hp_pos[0] - 1.0 * self.player2.hp / Unit.DEFAULT_HP * self.hp_full_length, self.player2_hp_pos[1], 1.0 * self.player2.hp / Unit.DEFAULT_HP * self.hp_full_length, self.hp_full_height])

        self.display.blit(self.score_screen, (0, 0))
        self.display.blit(self.game_screen, (0, self.score_height))
        pygame.display.update()

    def _render_debug(self):
        pygame.draw.rect(self.game_screen, (255,0,0), pygame.Rect((self.player1.center - self.player1.size / 2.0).x, (self.player1.center - self.player1.size / 2.0).y, self.player1.size.x, self.player1.size.y), 1)
        pygame.draw.rect(self.game_screen, (255,0,0), pygame.Rect((self.player2.center - self.player2.size / 2.0).x, (self.player2.center - self.player2.size / 2.0).y, self.player2.size.x, self.player2.size.y), 1)
        for projectile in self.projectiles:
            pygame.draw.rect(self.game_screen, (255,0,0), pygame.Rect((projectile.center - projectile.size / 2.0).x, (projectile.center - projectile.size / 2.0).y, projectile.size.x, projectile.size.y), 1)

    def start(self):
        self._init_sprites()
        self._init_units()
        self._init_game()
        self._render()
        self.AI.notify_game_start(self.get_screen())
        self.state = Spaceships.States.PLAYING
        while self.state != Spaceships.States.QUIT and \
              self.state != Spaceships.States.FINISHED:
            self._manage_input()
            self._manage_collisions()
            self._render()
            self._manage_events()
            self.frames += 1
            if not self.args.training:
                self.clock.tick_busy_loop(self.framerate)
        if self.state == Spaceships.States.FINISHED:
            self.AI.notify_game_end()

def main():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--resolution', type=str, choices=['small', 'medium'], default='medium')
    parser.add_argument('--framerate', type=int, default=60)
    parser.add_argument('--fullscreen', action='store_true')
    parser.add_argument('--debug', action='store_true')

    parser.add_argument('--player1', type=str, choices=['human', 'dqn', 'deterministic'], default='human', help='TODO')
    parser.add_argument('--player2', type=str, choices=['human', 'dqn', 'deterministic'], default='human', help='TODO')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--test-steps', type=int, default=10000, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=1000000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-path', type=str, default='memory.npy', help='TODO')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memory', type=str, help='TODO')
    parser.add_argument('--training', action='store_true', help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--log', type=str, help='TODO')

    args = parser.parse_args()
    args.image_shape = [None, 84, 84, 4]
    args.state_size = args.image_shape[-1]
    print args

    spaceships = Spaceships(args)
    spaceships.start()

    while args.training:
        spaceships.start()

if __name__ == '__main__':
    main()
