import sys
import os
import sys
import random
import time
import argparse
import copy
import signal

import numpy as np
import scipy.ndimage
import pygame
import shapely.affinity
import shapely.geometry

from sprite_strip_anim import SpriteStripAnim
from DQN import DQN
from Memory import Memory

class Point:
    def __init__(self, x, y):
        self.x, self.y = x, y
    def __neg__(self):
        return Point(-self.x, -self.y)
    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.x + other[0], self.y + other[1])
        raise NotImplementedError('+ operator for Point requires either Point or (x, y) tuple.')
    def __sub__(self, other):
        if isinstance(other, Point):
            return Point(self.x - other.x, self.y - other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.x - other[0], self.y - other[1])
        raise NotImplementedError('- operator for Point requires either Point or (x, y) tuple.')
    def __mul__(self, other):
        if isinstance(other, Point):
            return Point(self.x * other.x, self.y * other.y)
        elif isinstance(other, tuple) and \
             len(other) == 2:
            return Point(self.y * other[0], self.y * other[0])
        elif isinstance(other, (int, float, long, complex)):
            return Point(self.x * other, self.y * other)
        raise NotImplementedError('* operator for Point requires either Point, (x, y) tuple or number.')
    def __div__(self, s):
        return Point(1.0 * self.x / s, 1.0 * self.y / s)
    def __abs__(self):
        return Point(np.abs(self.x), np.abs(self.y))
    def __iter__(self):
        yield self.x
        yield self.y
    def __repr__(self):
        return '(%r, %r)' % (self.x, self.y)

class Angle:
    FULL_CIRCLE = 2 * np.pi
    def __init__(self, angle):
        self.angle = angle % Angle.FULL_CIRCLE
    def __neg__(self):
        return Angle((-self.angle) % Angle.FULL_CIRCLE)
    def __add__(self, other):
        if isinstance(other, Angle):
            return Angle((self.angle + other.angle) % Angle.FULL_CIRCLE)
        elif isinstance(other, (int, float, long, complex)):
            return Angle((self.angle + other) % Angle.FULL_CIRCLE)
        raise NotImplementedError('+ operator for Angle requires either Angle or number.')
    def __sub__(self, other):
        if isinstance(other, Angle):
            return Angle((self.angle - other.angle) % Angle.FULL_CIRCLE)
        elif isinstance(other, (int, float, long, complex)):
            return Angle((self.angle - other) % Angle.FULL_CIRCLE)
        raise NotImplementedError('+ operator for Angle requires either Angle or number.')
    def __mul__(self, s):
        return Angle((self.angle * s) % Angle.FULL_CIRCLE)
    def __div__(self, s):
        return Angle((self.angle / s) % Angle.FULL_CIRCLE)
    def __repr__(self):
        return '%r' % (self.angle)
    def in_radians(self):
        return self.angle
    def in_degrees(self):
        return self.angle * 180.0 / np.pi

class Unit():

    DEFAULT_VELOCITY = Point(0,0)
    DEFAULT_SPEED = 0.1
    DEFAULT_VELOCITY_DECAY = 0.99
    DEFAULT_ROTATION_ANGLE = np.pi / 45 #5 degrees
    DEFAULT_HP = 1000
    DEFAULT_PROJECTILE_DAMAGE = 100

    class Roles:
        PLAYER = 11
        PLAYER1 = 11
        PLAYER2 = 22
        WALL = 13
        PROJECTILE = 17

    class Keys:
        ACCELERATE = 0
        LEFT = 1
        RIGHT = 2
        FIRE = 3

    def __init__(self, role, center, rotation, size, anims, keys=None, static=False):
        self.role = role
        self.center = center
        self.rotation = rotation
        self.size = size
        self.keys = keys
        self.anims = anims
        self.static = static
        if self.role % Unit.Roles.PROJECTILE == 0:
            self.speed = 0
            self.velocity = Point(np.cos(self.rotation.in_radians()), -np.sin(self.rotation.in_radians())) * 10
            self.velocity_decay = 1
            self.damage = Unit.DEFAULT_PROJECTILE_DAMAGE
            self.moving = True
        else:
            self.speed = Unit.DEFAULT_SPEED
            self.velocity = Unit.DEFAULT_VELOCITY
            self.velocity_decay = Unit.DEFAULT_VELOCITY_DECAY
            self.moving = False
        self.fired = 0
        self.rotation_speed = Unit.DEFAULT_ROTATION_ANGLE
        self.hp = Unit.DEFAULT_HP
        self.collision_box = shapely.geometry.Polygon([(0, 0), (self.size.y, 0), (self.size.y, self.size.x), (0, self.size.x)])

    def alive(self):
        return self.hp > 0

    def get_collision_box(self):
        cb = self.collision_box
        c = cb.centroid.coords[0]
        cb = shapely.affinity.translate(cb, *tuple(self.center - c))
        cb = shapely.affinity.rotate(cb, -self.rotation.in_degrees())
        return cb

    def reset_anim(self, moving):
        self.anim_idx = 0

    def move(self):
        self.center += self.velocity
        self.velocity *= self.velocity_decay

    def push(self):
        self.moving = True
        self.velocity += (self.speed * np.cos(self.rotation.in_radians()), self.speed * -np.sin(self.rotation.in_radians()))

    def rotate(self, angle):
        self.rotation += angle

    def rotate_left(self):
        self.rotate(self.rotation_speed)

    def rotate_right(self):
        self.rotate(-self.rotation_speed)

class SpaceshipsAI:

    class PlayerAuxiliary:
        def __init__(self, ai, s0=None, a=None, r=None, s1=None, terminal=None, frame=None, total_reward=0, acting=0, active=False):
            self.ai = ai
            self.s0 = s0
            self.a = a
            self.r = r
            self.s1 = s1
            self.terminal = terminal
            self.total_reward = total_reward
            self.acting = acting
            self.active = active
        def __repr__(self):
            return '%r' % (self.__dict__)

    def saver(self, *args, **kwargs):
        print('SIGINT. Saving...')
        if self.args.model_save_path:
            self.model.save(self.args.model_save_path, self.memory.iteration)
        if self.args.memory_save_path: 
            self.memory.save(self.args.memory_save_path)
        sys.exit(0)

    def __init__(self, spaceships, args):
        self.spaceships = spaceships
        self.args = args
        
        self.action_space = [Unit.Keys.ACCELERATE, Unit.Keys.FIRE, Unit.Keys.LEFT, Unit.Keys.RIGHT, -1]
        self.model = DQN(self.args.image_shape, len(self.action_space))
        if self.args.model:
            self.model.restore(self.args.model)

        if self.args.memory:
            self.memory = Memory.load(self.args.memory)
        else:
            self.memory = Memory(self.args.memory_size, len(self.action_space))
        self.memory.player1_active = True
        signal.signal(signal.SIGINT, self.saver)

        if self.args.training:
            self.epsilon = lambda i: max(0.1, 1 - 0.90 * i / 1000000)
        else:
            self.epsilon = lambda _: self.args.test_epsilon
        self.grayscale = np.array([.299, .587, .114])
        self.ndzoom_shape = (1.0 * self.args.image_shape[1] / self.spaceships.height, 1.0 * self.args.image_shape[2] / self.spaceships.width)
        self.strs = []
        self.iters = 0

    def _preprocess(self, img):
        frame = np.dot(img, self.grayscale).astype(np.uint8)
        frame = scipy.ndimage.zoom(frame, self.ndzoom_shape)
        frame = np.resize(frame, (self.args.image_shape[1], self.args.image_shape[2], 1))
        return frame

    def _get_player(self, role):
        return self.player1 if role == Unit.Roles.PLAYER1 else self.player2

    def is_ai(self, player):
        return self.player1.ai if player.role == Unit.Roles.PLAYER1 else self.player2.ai

    def notify_game_start(self, screen):
        self.state = []
        for _ in range(self.args.state_size):
            self.state.append(self._preprocess(screen))
        self.player1 = SpaceshipsAI.PlayerAuxiliary(self.args.player1_ai, active=self.memory.player1_active)
        self.player2 = SpaceshipsAI.PlayerAuxiliary(self.args.player2_ai, active=(not self.memory.player1_active))
        self.starting_iteration = self.memory.iteration
        self.t0 = time.time()

    def get_action(self, role, screen):
        player = self._get_player(role)
        if not player.ai:
            return None
        if player.acting == self.args.frame_skip:
            if player.s0 == None:
                player.s1 = self.state[:-1]
            else:
                player.s1 = player.s0[:-1]
            player.s1.insert(0, self._preprocess(player.frame))
            if self.args.training and player.active and player.s0 != None:
                self.memory.add((player.s0, player.a, player.r, player.s1, player.terminal))
                if (self.args.memory_init != None and self.memory.iteration > self.args.memory_init) and \
                   self.args.train_wait != None and self.memory.iteration % self.args.train_wait == 0:
                    batch = self.memory.sample(self.args.batch_size)
                    self.model.train(batch)
            if player.terminal:
                player.s0 = None
            else:
                player.s0 = player.s1
            if self.args.training and player.active and \
               (self.args.memory_init is None or self.memory.iteration > self.args.memory_init):
                if self.args.model_save_path and self.memory.iteration % self.args.model_save_freq == 0:
                    if self.args.verbose: print('Saving model.')
                    self.model.save(self.args.model_save_path, self.memory.iteration)
                if self.args.memory_save_path and self.memory.iteration % self.args.memory_save_freq == 0:
                    if self.args.verbose: print('Saving memory.')
                    self.memory.save(self.args.memory_save_path)
                if self.args.model_update_freq != None and self.memory.iteration % self.args.model_update_freq == 0:
                    if self.args.verbose: print('Updating target models.')
                    self.model.update_target()
            player.total_reward += player.r
            player.acting = 0
            if self.args.training and player.active:
                self.memory.iteration += 1
        elif player.acting > 0:
            player.frame = screen
            player.acting += 1
        if player.acting == 0:
            if player.s0 == None or random.random() < self.epsilon(self.memory.iteration):
                player.a = random.randrange(len(self.action_space))
            else:
                s = np.reshape(np.concatenate(player.s0, axis=2), (1, self.args.image_shape[1], self.args.image_shape[2], self.args.state_size))
                player.a = np.argmax(self.model.predict(s), axis=1)[0]
            player.frame = screen
            player.r = 0
            player.terminal = False
            player.acting += 1
        return self.action_space[player.a]

    def notify_reward(self, role, reward):
        player = self._get_player(role)
        if not player.ai:
            return None
        player.r += reward

    def notify_terminal(self, screen):
        self.player1.terminal = True
        self.player1.frame = screen
        self.player1.acting = self.args.frame_skip
        self.player2.terminal = True
        self.player2.frame = screen
        self.player2.acting = self.args.frame_skip

    def notify_game_end(self):
        self.t1 = time.time()
        self.memory.episode += 1
        self.memory.player1_active = not self.memory.player1_active
        self.iters += self.memory.iteration
        self.strs.append('PLAYER 1    EP %d    IT %d    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, self.player1.total_reward, self.t1 - self.t0, (1.0 * self.memory.iteration - self.starting_iteration) / (self.t1 - self.t0)))
        self.strs.append('PLAYER 2    EP %d    IT %d    R=%d    %.4fs (%.4f IPS)' % (self.memory.episode, self.memory.iteration, self.player2.total_reward, self.t1 - self.t0, (1.0 * self.memory.iteration - self.starting_iteration) / (self.t1 - self.t0)))
        if self.args.verbose:
            print self.strs[-2]
            print self.strs[-1]
        if self.args.train_steps != None and \
           self.iters > self.args.train_steps:
            self.iters = 0
            if self.args.log:
                try: 
                    with open(self.args.log, 'a') as f:
                        f.write('\n'.join(strs))
                except Exception as e:
                    print 'Couldn\'t save log: ' + e.message

class Spaceships:

    class Events:
        BASE = pygame.USEREVENT
        PLAYER_DEATH = BASE + 1
        PLAYERS_DEATH = BASE + 2
        PLAYER_FIRE = BASE + 3
        PLAYER_HIT = BASE + 4
	PROJECTILE_DEATH = BASE + 5

    class States:
        LOADING = 0
        PLAYING = 1
        PAUSED = 2
        FINISHED = 3
        QUIT = 4

    def __init__(self, args):

        self.args = args

        if self.args.resolution == 'medium':
            self.width, self.height = 600, 400 
        elif self.args.resolution == 'small':
            self.width, self.height = 300, 200
        self.score_width, self.score_height = self.width, int(0.05 * self.height)
        self.framerate = self.args.framerate

        if self.args.training:
            os.environ['SDL_VIDEODRIVER'] = 'dummy'
        pygame.init()
        self.display = pygame.display.set_mode((self.width, self.height + self.score_height), pygame.FULLSCREEN if args.fullscreen else 0)
        self.score_screen = pygame.Surface((self.score_width, self.score_height))
        self.game_screen = pygame.Surface((self.width, self.height))

        if self.args.resolution == 'medium':
            self._load_sprites('games_aux/medium', 5, (16, 24), (5, 13), (16, 24), 18)
        elif self.args.resolution == 'small':
            self._load_sprites('games_aux/small', 5, (8, 12), (3, 6), (8, 12), 9)
        self._load_ai()

    def _load_sprites(self, base_dir, frames, ship_size, projectile_size, explosion_size, font_size):
        background = pygame.image.load(base_dir + '/background.png').convert()
        self.background = pygame.Surface((self.width, self.height)).convert()
        for i in range(0, self.width, background.get_size()[0]):
            for j in range(0, self.height, background.get_size()[1]):
                self.background.blit(background, (i, j))

        self.ship1_active_anim = SpriteStripAnim(base_dir + '/ship1_active.png', (0, 0, ship_size[0], ship_size[1]), 5, loop=True, frames=frames)
        for i, image in enumerate(SpriteStripAnim(base_dir + '/ship1_active.png', (0, ship_size[1], ship_size[0], ship_size[1]), 5, loop=True, frames=frames).images):
            self.ship1_active_anim.images.insert(i * 2 + 1, image)
        self.ship1_inactive_anim = SpriteStripAnim(base_dir + '/ship1_inactive.png', (0, 0, ship_size[0], ship_size[1]), 1, loop=True, frames=frames)

        self.ship2_active_anim = SpriteStripAnim(base_dir + '/ship2_active.png', (0, 0, ship_size[0], ship_size[1]), 5, loop=True, frames=frames)
        for i, image in enumerate(SpriteStripAnim(base_dir + '/ship2_active.png', (0, ship_size[1], ship_size[0], ship_size[1]), 5, loop=True, frames=frames).images):
            self.ship2_active_anim.images.insert(i * 2 + 1, image)
        self.ship2_inactive_anim = SpriteStripAnim(base_dir + '/ship2_inactive.png', (0, 0, ship_size[0], ship_size[1]), 1, loop=True, frames=frames)

        self.player_projectile_anim = SpriteStripAnim(base_dir + '/laser-bolts.png', (0, 0, projectile_size[0], projectile_size[1]), 2, loop=True, frames=frames*5)
        self.explosion_anim = SpriteStripAnim(base_dir + '/explosion.png', (0, 0, explosion_size[0], explosion_size[1]), 5, loop=False, frames=frames)

        self.ship_size = Point(*ship_size)
        self.projectile_size = Point(*projectile_size)

        self.font = pygame.font.Font(base_dir + '/PixelFont.ttf', font_size)
        self.player1_tag = self.font.render('1', True, (255, 255, 255))
        self.player1_tag_pos = (0.05 * self.score_width, (self.score_height - self.player1_tag.get_size()[1]) / 2)
        self.player2_tag = self.font.render('2', True, (255, 255, 255))
        self.player2_tag_pos = (0.95 * self.score_width - self.player2_tag.get_size()[0], (self.score_height - self.player2_tag.get_size()[1]) / 2)

        self.player1_hp_pos = (self.player1_tag_pos[0] + self.player1_tag.get_size()[0] + 10, self.player1_tag_pos[1])
        self.player2_hp_pos = (self.player2_tag_pos[0] - 10, self.player2_tag_pos[1])
        self.hp_full_length = 0.425 * (self.score_width - self.player1_tag.get_size()[0] - self.player2_tag.get_size()[0] - 20)
        self.hp_full_height = 0.75 * self.player1_tag.get_size()[1]

    def _load_ai(self):
        self.AI = SpaceshipsAI(self, self.args)

    def _init_sprites(self):
        self.ship1_active_anim.iter()
        self.ship1_inactive_anim.iter()
        self.ship2_active_anim.iter()
        self.ship2_inactive_anim.iter()
        self.explosion_anim.iter()

    def _init_units(self):
        self.player1 = Unit(
            Unit.Roles.PLAYER1, 
            Point(0.2 * self.width, 0.75 * self.height), 
            Angle(np.pi / 2), 
            self.ship_size, 
            anims={True: self.ship1_active_anim, False: self.ship1_inactive_anim}, 
            keys={Unit.Keys.ACCELERATE: pygame.K_w, Unit.Keys.LEFT: pygame.K_a, Unit.Keys.RIGHT: pygame.K_d, Unit.Keys.FIRE: pygame.K_LSHIFT})
        self.player2 = Unit(
            Unit.Roles.PLAYER2, 
            Point(0.8 * self.width, 0.75 * self.height), 
            Angle(np.pi / 2), 
            self.ship_size, 
            anims={True: self.ship2_active_anim, False: self.ship2_inactive_anim}, 
            keys={Unit.Keys.ACCELERATE: pygame.K_UP, Unit.Keys.LEFT: pygame.K_LEFT, Unit.Keys.RIGHT: pygame.K_RIGHT, Unit.Keys.FIRE: pygame.K_RSHIFT})
        self.players = {
            Unit.Roles.PLAYER1: self.player1,
            Unit.Roles.PLAYER2: self.player2
        }
        self.walls = [
            shapely.geometry.LineString([[0, 0], [0, self.height]]),
            shapely.geometry.LineString([[0, self.height], [self.width, self.height]]),
            shapely.geometry.LineString([[self.width, self.height], [self.width, 0]]),
            shapely.geometry.LineString([[self.width, 0], [0, 0]])
        ]
        self.walls_aux_offset = 50
        self.walls_aux = [
            shapely.geometry.LineString([[-self.walls_aux_offset, 0], [-self.walls_aux_offset, self.height]]),
            shapely.geometry.LineString([[0, self.height + self.walls_aux_offset], [self.width, self.height + self.walls_aux_offset]]),
            shapely.geometry.LineString([[self.width + self.walls_aux_offset, self.height], [self.width + self.walls_aux_offset, 0]]),
            shapely.geometry.LineString([[self.width, -self.walls_aux_offset], [0, -self.walls_aux_offset]])
        ]
        self.projectiles = []
        self.explosions = []

    def _init_game(self):
        self.clock = pygame.time.Clock()
        self.state = Spaceships.States.LOADING
        self.score = {Unit.Roles.PLAYER1: 0, Unit.Roles.PLAYER2: 0}
        self.frames = 0
        self.e_tt = 0
        self.i_tt = 0
        self.c_tt = 0
        self.g_tt = 0
        self.r_tt = 0

    def _finish_game(self):
        self.state = Spaceships.States.FINISHED

    def _manage_events(self):
        t0 = time.time()
        if self.frames >= 30 * self.framerate:
            self.player1.hp -= 1
            self.player2.hp -= 1
            self.AI.notify_reward(Unit.Roles.PLAYER1, -1)
            self.AI.notify_reward(Unit.Roles.PLAYER2, -1)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.state = Spaceships.States.QUIT
            elif event.type == Spaceships.Events.PLAYER_DEATH:
                self._finish_game()
                self.AI.notify_terminal(self.get_screen())
            elif event.type == pygame.KEYUP:
                if event.key == self.player1.keys[Unit.Keys.FIRE]:
                    self.player1.fired = 0
                if event.key == self.player2.keys[Unit.Keys.FIRE]:
                    self.player2.fired = 0
        self.e_tt += time.time() - t0

    def _manage_input(self):
        t0 = time.time()
        keys = pygame.key.get_pressed()
        if keys[pygame.K_ESCAPE]:
            pygame.event.post(pygame.event.Event(pygame.QUIT))
        for player in self.players.itervalues():
            if not player.alive():
                pygame.event.post(pygame.event.Event(Spaceships.Events.PLAYER_DEATH, player=player))
                continue
            player.moving = False
            action = None
            is_ai = self.AI.is_ai(player)
            t1 = time.time()
            action = self.AI.get_action(player.role, self.get_screen())
            self.g_tt += time.time() - t1
            if (not is_ai and keys[player.keys[Unit.Keys.ACCELERATE]]) or \
               action == Unit.Keys.ACCELERATE:
                player.push()
            if (not is_ai and keys[player.keys[Unit.Keys.LEFT]]) or \
               action == Unit.Keys.LEFT:
                player.rotate_left()
            if (not is_ai and keys[player.keys[Unit.Keys.RIGHT]]) or \
               action == Unit.Keys.RIGHT:
                player.rotate_right()
            if (not is_ai and keys[player.keys[Unit.Keys.FIRE]]) or \
               action == Unit.Keys.FIRE:
                if player.fired > 0:
                    player.fired -= 1
                else:
                    player.fired = self.framerate / 5
                    projectile = Unit(Unit.Roles.PROJECTILE, player.center, player.rotation, self.projectile_size, {True: self.player_projectile_anim})
                    projectile.originator = player
                    projectile.order = len(self.projectiles)
                    self.projectiles.append(projectile)
            if not player.moving:
                player.anims[False].iter()
        self.i_tt += time.time() - t0

    def _manage_collisions(self):
        t0 = time.time()
        for player in self.players.itervalues():
            player.move()
        for projectile in self.projectiles:
            projectile.move()
        if self.player1.get_collision_box().intersects(self.player2.get_collision_box()):
            pass
        for player in self.players.itervalues():
            cb = player.get_collision_box()
            for i, wall in enumerate(self.walls):
                if cb.intersects(wall):
                    dist = cb.distance(self.walls_aux[i]) - self.walls_aux_offset
                    if i == 0:
                        player.center -= (dist, 0)
                    elif i == 1:
                        player.center -= (0, -dist)
                    elif i == 2:
                        player.center -= (-dist, 0)
                    elif i == 3:
                        player.center -= (0, dist)
            for i in xrange(len(self.projectiles) - 1, -1, -1):
                if self.projectiles[i].originator != player and \
                   cb.intersects(self.projectiles[i].get_collision_box()):
                    explosion = (self.explosion_anim, player.center, player.rotation)
                    explosion[0].iter()
                    self.explosions.append(explosion)
                    player.hp -= self.projectiles[i].damage
                    self.AI.notify_reward(player.role, -self.projectiles[i].damage)
                    self.AI.notify_reward((1 + ((player.role / Unit.Roles.PLAYER) % 2)) * Unit.Roles.PLAYER, self.projectiles[i].damage)
                    try: del self.projectiles[i]
                    except IndexError: pass
        for i in xrange(len(self.projectiles) - 1, -1, -1):   
            cb = self.projectiles[i].get_collision_box()
            for wall in self.walls_aux:
                if cb.intersects(wall):
                    try: del self.projectiles[i]
                    except IndexError: pass
        self.c_tt += time.time() - t0

    def _blit_unit(self, surface, unit):
        image = pygame.transform.rotate(unit.anims[unit.moving].next(), (unit.rotation - np.pi/2).in_degrees())
        rect = image.get_rect()
        rect.center = tuple(unit.center)
        surface.blit(image, rect)

    def get_screen(self):
        return np.transpose(pygame.surfarray.array3d(self.display), axes=[1, 0, 2])

    def _render(self):
        t0 = time.time()
        self.game_screen.blit(self.background, (0, 0))
        for projectile in self.projectiles:
            self._blit_unit(self.game_screen, projectile)
        if self.player1.alive(): 
            self._blit_unit(self.game_screen, self.player1)
        if self.player2.alive(): 
            self._blit_unit(self.game_screen, self.player2)    
        for i in xrange(len(self.explosions) - 1, -1, -1):
            try: 
                image = pygame.transform.rotate(self.explosions[i][0].next(), (self.explosions[i][2] - np.pi/2).in_degrees())
                rect = image.get_rect()
                rect.center = tuple(self.explosions[i][1])
                self.game_screen.blit(image, rect)
            except StopIteration:
                del self.explosions[i]
        if self.args.debug:
            self._render_debug()
        
        self.score_screen.fill((0,0,0))
        self.score_screen.blit(self.player1_tag, self.player1_tag_pos)
        pygame.draw.rect(self.score_screen, (0, 200, 0), [self.player1_hp_pos[0], self.player1_hp_pos[1], 1.0 * self.player1.hp / Unit.DEFAULT_HP * self.hp_full_length, self.hp_full_height])
        self.score_screen.blit(self.player2_tag, self.player2_tag_pos)
        pygame.draw.rect(self.score_screen, (0, 200, 0), [self.player2_hp_pos[0] - 1.0 * self.player2.hp / Unit.DEFAULT_HP * self.hp_full_length, self.player2_hp_pos[1], 1.0 * self.player2.hp / Unit.DEFAULT_HP * self.hp_full_length, self.hp_full_height])

        self.display.blit(self.score_screen, (0, 0))
        self.display.blit(self.game_screen, (0, self.score_height))
        pygame.display.update()
        self.r_tt += time.time() - t0

    def _render_debug(self):
        pygame.draw.polygon(self.game_screen, (255,0,0), self.player1.get_collision_box().exterior.coords, 1)
        pygame.draw.polygon(self.game_screen, (255,0,0), self.player2.get_collision_box().exterior.coords, 1)
        for projectile in self.projectiles:
            pygame.draw.polygon(self.game_screen, (255,0,0), projectile.get_collision_box().exterior.coords, 1)

    def start(self):
        self._init_sprites()
        self._init_units()
        self._init_game()
        self._render()
        self.AI.notify_game_start(self.get_screen())
        self.state = Spaceships.States.PLAYING
        while self.state != Spaceships.States.QUIT and \
              self.state != Spaceships.States.FINISHED:
            self._manage_input()
            self._manage_collisions()
            self._render()
            self._manage_events()
            self.frames += 1
            if not self.args.training:
                self.clock.tick_busy_loop(self.framerate)
        tt = self.e_tt + self.i_tt + self.c_tt + self.r_tt
        print 'EVENTS: %.4f (%.6f%%)' % (self.e_tt, 100.0 * self.e_tt / tt)
        print 'INPUT: %.4f (%.6f%%)' % (self.i_tt, 100.0 * self.i_tt / tt)
        print 'GET ACTION: %.4f (%.6f%%)' % (self.g_tt, 100.0 * self.g_tt / tt)
        print 'COLLISIONS: %.4f (%.6f%%)' % (self.c_tt, 100.0 * self.c_tt / tt)
        print 'RENDER: %.4f (%.6f%%)' % (self.r_tt, 100.0 * self.r_tt / tt)
        if self.state == Spaceships.States.FINISHED:
            self.AI.notify_game_end()

def main():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--resolution', type=str, choices=['small', 'medium'], default='medium')
    parser.add_argument('--framerate', type=int, default=60)
    parser.add_argument('--fullscreen', action='store_true')
    parser.add_argument('--debug', action='store_true')

    parser.add_argument('--player1-ai', action='store_true', help='TODO')
    parser.add_argument('--player2-ai', action='store_true', help='TODO')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--test-steps', type=int, default=10000, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=1000000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-path', type=str, default='memory.npy', help='TODO')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memory', type=str, help='TODO')
    parser.add_argument('--training', action='store_true', help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--log', type=str, help='TODO')

    args = parser.parse_args()
    args.image_shape = [None, 84, 84, 4]
    args.state_size = args.image_shape[-1]
    print args

    spaceships = Spaceships(args)
    spaceships.start()

    while args.training:
        spaceships.start()

if __name__ == '__main__':
    main()
