import numpy as np
import tensorflow as tf
from functools import partial
import time

class DQN:

    def __init__(self, input_shape, num_actions, num_tasks, network_shapes=[[[8,8,4,32], [1,4,4,1]], [[4,4,32,64], [1,2,2,1]], [[3,3,64,64], [1,1,1,1]], 512], graph=tf.Graph()):

        tf.set_random_seed(123456)

        self.input_shape = input_shape
        self.num_actions = num_actions
        self.num_tasks = num_tasks
        self.learning_rate = 0.00025
        self.lbd = 400
        self.gamma = 0.99

        self.graph = graph
        with self.graph.as_default():
            self.sess = tf.Session()

            self.x = tf.placeholder(tf.uint8, input_shape, name='screens')
            self.normalized = tf.to_float(self.x) / 255.0
            
            self.tf_look_up = tf.constant(np.arange(num_tasks))
            self.network_shapes = network_shapes
            self.network_shapes.insert(0, input_shape)
            self.network_shapes.append(num_actions)
            self.y = self._build_network('policy', self.normalized, self.network_shapes, True)
            self.y_ = self._build_network('target', self.normalized, self.network_shapes, False)

            self.update_target_model = []
            self.trainable_vars = tf.trainable_variables()
            self.trainable_weights = [var for var in self.trainable_vars if '/w' in var.name]
            global_vars = tf.global_variables()
            for i in range(len(self.trainable_vars)):
                match = list(filter(lambda var: var is not self.trainable_vars[i] and var.name.split('/')[1:] == self.trainable_vars[i].name.split('/')[1:], global_vars))[0]
                self.update_target_model.append(tf.assign(match, self.trainable_vars[i]))

            self.a = tf.placeholder(tf.float32, [None, self.num_actions], name='actions')
            self.t = tf.placeholder(tf.float32, [None], name='targets')

            self.previous_task_weights = [tf.placeholder(v.dtype, v.get_shape()) for v in self.trainable_weights]
            self.fisher = [tf.placeholder(v.dtype, v.get_shape().as_list()) for v in self.trainable_weights]

            self.deriv_which = tf.placeholder(tf.int32, ())
            prob = tf.nn.softmax(tf.gather(self.y, self.tf_look_up[self.deriv_which]))
            action_random = tf.to_int32(tf.multinomial(tf.log(prob), 1)[0][0])
            self.deriv = tf.gradients(tf.log(prob[0, action_random]), self.trainable_weights)

            self.y_a, self.y_a_ = [], []
            self.fisher_matrixes = []
            self.loss, self.EWC_loss = [], []
            for i in range(self.num_tasks):
                with tf.variable_scope('task_%d' % (i)):
                    self.y_a.append(tf.multiply(self.y[i], self.a))
                    self.y_a_.append(tf.multiply(self.y_[i], self.a))
                    
                    diff = tf.abs(tf.reduce_sum(self.y_a[i], axis=1) - self.t)
                    quadratic = tf.clip_by_value(diff, 0.0, 1.0)
                    linear = diff - quadratic
                    errors = (0.5 * tf.square(quadratic)) + linear
                    self.loss.append(tf.reduce_sum(errors))
                    
                    self.EWC_loss.append(0) 
                    for j, var in enumerate(self.trainable_weights):
                        self.EWC_loss[i] += tf.reduce_sum(tf.multiply(tf.cast(self.fisher[j], tf.float32), tf.square(var - self.previous_task_weights[j])))
                    self.EWC_loss[i] *= self.lbd / 2.0

            self.final_loss = [self.loss[i] + self.EWC_loss[i] for i in range(self.num_tasks)]

            self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate, decay=0.95, epsilon=0.01, name='optimizer')
            self.train_step = [self.optimizer.minimize(self.loss[i]) for i in range(self.num_tasks)]
            self.EWC_train_step = [self.optimizer.minimize(self.final_loss[i]) for i in range(self.num_tasks)]
            
            self.saver = tf.train.Saver(max_to_keep=50)

            self.sess.run(tf.global_variables_initializer())

    def _build_network(self, name, x, shapes, trainable):
        Q_vals = []
        with tf.variable_scope(name):
            with tf.variable_scope('conv1'):
                a1 = self._get_activated(shapes[1][0], 
                                         activation=tf.nn.relu, 
                                         inputs=[x] * self.num_tasks,
                                         op=partial(tf.nn.conv2d, strides=shapes[1][1], padding='VALID'),
                                         argnames=['input', 'filter'], 
                                         trainable=trainable)
            with tf.variable_scope('conv2'):
                a2 = self._get_activated(shapes[2][0], 
                                         activation=tf.nn.relu,
                                         inputs=a1,
                                         op=partial(tf.nn.conv2d, strides=shapes[2][1], padding='VALID'), 
                                         argnames=['input', 'filter'], 
                                         trainable=trainable)
            with tf.variable_scope('conv3'):
                a3 = self._get_activated(shapes[3][0], 
                                         activation=tf.nn.relu,
                                         inputs=a2,
                                         op=partial(tf.nn.conv2d, strides=shapes[3][1], padding='VALID'), 
                                         argnames=['input', 'filter'], 
                                         trainable=trainable)
            w, h = shapes[0][1], shapes[0][2]
            for i in range(1,4):
                w, h = (w - shapes[i][0][0]) / shapes[i][1][1] + 1, (h - shapes[i][0][1]) / shapes[i][1][2] + 1
            flat = []
            for i in range(self.num_tasks):
                with tf.variable_scope('task_%d' % (i)):
                    flat.append(tf.reshape(a3[i], [-1, w*h*shapes[3][0][-1]], name='flattened'))
            with tf.variable_scope('fc1'):
                a4 = self._get_activated([w*h*shapes[3][0][-1], shapes[4]], 
                                         activation=tf.nn.relu,
                                         inputs=flat,
                                         op=partial(tf.matmul),
                                         trainable=trainable)
            with tf.variable_scope('fc2'):
                Q_vals = self._get_activated([shapes[4], shapes[5]], 
                                             activation=tf.identity,
                                             inputs=a4, 
                                             op=partial(tf.matmul),
                                             trainable=trainable)
        return Q_vals

    def _get_activated(self, shape, activation, inputs, op, argnames=['a', 'b'], trainable=True):
        a = []
        w = tf.Variable(tf.truncated_normal(shape, stddev=0.01), trainable=trainable, name='w')
        for i in range(self.num_tasks):
            with tf.variable_scope('task_%d' % (i)):
                b = tf.Variable(tf.fill([shape[-1]], 1e-6), trainable=trainable, name='b')
                g = tf.Variable(tf.fill([shape[-1]], 1.0), trainable=trainable, name='g')
                a.append(activation((op(**{argnames[0]: inputs[i], argnames[1]: w}) + b) * g, name='a'))
        return a

    def predict(self, states, which):
        with self.graph.as_default():
            return self.sess.run(self.y[which], feed_dict={self.x: states})

    @staticmethod
    def to_one_hot(a, v_max):
        b = np.zeros((a.size, v_max+1))
        b[np.arange(a.size), a] = 1
        return b

    def compute_fisher(self, which, states):
        fisher = []
        for var in self.trainable_weights:
            fisher.append(np.zeros(var.get_shape().as_list()))
        for i in range(states.shape[0]):
            deriv = self.sess.run(self.deriv, feed_dict={self.x: states[i:i+1], self.deriv_which: which})
            for j, var in enumerate(self.trainable_weights):
                fisher[j] += np.square(deriv[j])
        for j in range(len(self.trainable_weights)):
            fisher[j] /= states.shape[0]
        return fisher

    def update_fisher(self, fisher):
        self.Fisher = fisher

    def train(self, batch, which, previous_task_weights=None):
        start_states, actions, rewards, next_states, terminal = batch
        with self.graph.as_default():
            Q_vals = self.sess.run(self.y_[which], feed_dict={self.x: next_states})
            Q_vals[terminal] = 0
            Q_vals = rewards + self.gamma * np.max(Q_vals, axis=1)
            feed_dict = {self.x: start_states, self.a: actions, self.t: Q_vals}
            if previous_task_weights != None:
                for i in range(len(self.previous_task_weights)):
                    feed_dict[self.previous_task_weights[i]] = previous_task_weights[i]
                    feed_dict[self.fisher[i]] = self.Fisher[i]
                self.sess.run(self.EWC_train_step[which], feed_dict=feed_dict)
            else:
                self.sess.run(self.train_step[which], feed_dict=feed_dict)

    def update_target(self):
        with self.graph.as_default():
            self.sess.run(self.update_target_model)

    def save(self, path, step):
        with self.graph.as_default():
            self.saver.save(self.sess, path, global_step=step)

    def restore(self, path):
        with self.graph.as_default():
            self.saver.restore(self.sess, path)
