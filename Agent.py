import argparse
import time
import sys
import os
import random
import signal
import subprocess

import numpy as np
from PIL import Image

from Environment import ALEEnvironment
from DQN import DQN
from Memory import Memory

def epoch(steps, env, dqn, memory, epsilon, args, training, EWC_weights=None):
    
    starting_iteration = memory.iteration
    strs = []

    while memory.iteration - starting_iteration < steps:
        
        total_reward = 0
        s0 = None

        env.reset()

        save_video = False
        frames = []
        iters = memory.iteration
        t0 = time.time()

        while not env.game_over():

            if s0 is None or random.random() < epsilon(memory.iteration):
                a = random.randrange(env.num_actions)
            else:
                s = np.reshape(np.concatenate(s0, axis=2), (1, args.image_shape[1], args.image_shape[2], args.state_size))
                if args.n != None:
                    a = np.argmax(dqn.predict(s, args.n), axis=1)[0]
                else:
                    a = np.argmax(dqn.predict(s), axis=1)[0]
            
            s1, r, terminal = env.act(a)

            if training and s0 is not None:
                memory.add((s0, a, r, s1, terminal))
                if (args.memory_init is None or memory.iteration > args.memory_init) and \
                   args.train_wait is not None and memory.iteration % args.train_wait is 0:
                    batch = memory.sample(args.batch_size)
                    if EWC_weights != None:
                        dqn.train(batch, args.n, EWC_weights)
                    elif args.n != None:
                        dqn.train(batch, args.n)
                    else:
                        dqn.train(batch)
            if terminal:
                s0 = None
            else:
                s0 = s1
                if args.video_save_path is not None:
                    frames.append(env.env.getScreenRGB())

            if training and (args.memory_init is None or memory.iteration > args.memory_init):
                if args.model_save_path is not None and memory.iteration % args.model_save_freq is 0:
                    if args.verbose: print('Saving model.')
                    dqn.save(args.model_save_path, memory.iteration)
                if args.memory_save_path is not None and memory.iteration % args.memory_save_freq is 0:
                    if args.verbose: print('Saving memory.')
                    memory.save(args.memory_save_path)
                if args.model_update_freq is not None and memory.iteration % args.model_update_freq is 0:
                    if args.verbose: print('Updating target model.')
                    dqn.update_target()
            if args.video_save_path is not None and memory.iteration % args.video_save_freq is 0:
                save_video = True

            total_reward += r
            memory.iteration += 1

        t1 = time.time()
        memory.episode += 1
        if save_video:
            if args.verbose: print('Saving video.')
            try:
                for i, frame in enumerate(frames):
                    Image.fromarray(frame).save('%s/frame-%08d.png' % (args.video_save_path, i))
                subprocess.check_output(('ffmpeg -r 30 -f image2 -s 1920x1080 -i %s/frame-%%08d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p %s/%d.mp4' % (args.video_save_path, args.video_save_path, memory.episode)).split(' '), stderr=subprocess.STDOUT)
                for i, _ in enumerate(frames):
                    os.remove('%s/frame-%08d.png' % (args.video_save_path, i))
            except subprocess.CalledProcessError as e:
                print('ffmpeg error: %r' % (e))
            except Exception as e:
                print('I/O exception: %r' % (e))
        strs.append('EP %d    IT %d (%5.2f%%)    R=%d    %.4fs (%.4f IPS)' % (memory.episode, memory.iteration, 100.0 * (memory.iteration - starting_iteration) / steps, total_reward, t1 - t0, (memory.iteration - iters) / (t1 - t0)))
        if args.verbose:
            print(strs[-1])
    
    if training:
        if args.log is not None:
            try: 
                with open(args.log, 'a') as f:
                    f.write('\n'.join(strs) + '\n')
            except Exception as e:
                print(e)

def main():
    
    random.seed(123456)

    parser = argparse.ArgumentParser()
    parser.add_argument('game', type=str, help='TODO')
    parser.add_argument('--train-steps', type=int, default=100000, help='TODO')
    parser.add_argument('--test-steps', type=int, default=10000, help='TODO')
    parser.add_argument('--train-wait', type=int, default=4, help='TODO')
    parser.add_argument('--frame-skip', type=int, default=4, help='TODO')
    parser.add_argument('--batch-size', type=int, default=32, help='TODO')
    parser.add_argument('--memory-size', type=int, default=1000000, help='TODO')
    parser.add_argument('--memory-init', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-freq', type=int, default=50000, help='TODO')
    parser.add_argument('--model-save-path', type=str, default='models1/model', help='TODO')
    parser.add_argument('--memory-save-freq', type=int, default=250000, help='TODO')
    parser.add_argument('--memory-save-path', type=str, default='memory.npy', help='TOOD')
    parser.add_argument('--model-update-freq', type=int, default=10000, help='TODO')
    parser.add_argument('--video-save-freq', type=int, default=100000, help='TODO')
    parser.add_argument('--video-save-path', type=str, default='videos', help='TOOD')
    parser.add_argument('--model', type=str, help='TODO')
    parser.add_argument('--memory', type=str, help='TODO')
    parser.add_argument('--test-epsilon', type=float, default=0.05, help='TODO')
    parser.add_argument('--verbose', action='store_true', help='TODO')
    parser.add_argument('--render', action='store_true', help='TODO')
    parser.add_argument('--log.txt', type=str, help='TODO')
    args = parser.parse_args()
    
    args.image_shape = [None, 84, 84, 4]
    args.state_size = args.image_shape[-1]

    env = ALEEnvironment(args.game, args.image_shape[1:3], args.state_size, args.frame_skip, args.render)

    dqn = DQN(args.image_shape, env.num_actions)
    if args.model is not None:
        dqn.restore(args.model)

    if args.memory is None:
        memory = Memory(args.memory_size, env.num_actions)
    else:
        memory = Memory.load(args.memory)

    epsilon_train = lambda i: max(0.1, 1 - 0.9 * i / 1000000)
    epsilon_test = lambda _: args.test_epsilon
    
    def saver(*a):
        print('SIGINT. Saving...')
        dqn.save(args.model_save_path, memory.iteration)
        if args.memory_save_path is not None: 
            memory.save(args.memory_save_path)
        sys.exit(0)
    def cleaner(*a):
        sys.exit(0)
    signal.signal(signal.SIGINT, saver)
    signal.signal(signal.SIGQUIT, cleaner)

    while True:
        if args.verbose:
            print('Beginning training for %d steps' % (args.train_steps))
        epoch(args.train_steps, env, dqn, memory, epsilon_train, args, training=True)
        if args.verbose:        
            print('Beginning testing for %d steps' % (args.test_steps))
        epoch(args.test_steps, env, dqn, memory, epsilon_test, args, training=False)

if __name__ == '__main__':
    main()